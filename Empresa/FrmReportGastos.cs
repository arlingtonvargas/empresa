﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmReportGastos : Form
    {
        public FrmReportGastos()
        {
            InitializeComponent();
        }

        private void FrmReportGastos_Load(object sender, EventArgs e)
        {
            try
            {
                CreaGrilla();
                dtpFechaIni.DateTime = DateTime.Now;
                dtpFechaFin.DateTime = DateTime.Now;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            if (dtpFechaIni.EditValue!=null && dtpFechaFin.EditValue!=null)
            {
                Consultar(Convert.ToDateTime(dtpFechaIni.EditValue), Convert.ToDateTime(dtpFechaFin.EditValue));
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CreaGrilla()
        {
            try
            {
                gvGastos = GrillaDevExpress.CrearGrilla(false, false, "");
                gvGastos.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvGastos.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvGastos.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvGastos.Columns.Add(GrillaDevExpress.CrearColumna("IdGasto", "Id", ancho:30));
                gvGastos.Columns.Add(GrillaDevExpress.CrearColumna("Descripcion", "Descripción", ancho:200));
                gvGastos.Columns.Add(GrillaDevExpress.CrearColumna("Fecha", "Id prod", ancho: 50, tipo: DevExpress.Utils.FormatType.DateTime, formato:"dd/MM/yyyy hh:mm tt"));
                gvGastos.Columns.Add(GrillaDevExpress.CrearColumna("valor", "Valor", ancho: 70, tipo: DevExpress.Utils.FormatType.Numeric, formato:"C2"));
                gvGastos.OptionsBehavior.Editable = true;
                gvGastos.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvGastos.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvGastos.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvGastos.OptionsCustomization.AllowColumnResizing = true;
                gvGastos.OptionsView.ShowFooter = true;
                gvGastos.Columns["valor"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                gvGastos.Columns["valor"].SummaryItem.FieldName = "valor";
                gvGastos.Columns["valor"].SummaryItem.DisplayFormat = "Total : {0:C2}";
                gcGastos.MainView = gvGastos;

            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void Consultar(DateTime fechaIni, DateTime fechaFin)
        {
            try
            {
                string sql = string.Format("set dateformat dmy;SELECT IdGasto, Descripcion, Fecha, valor " +
                    " FROM Gastos WHERE Fecha BETWEEN '{0:dd/MM/yyyy 00:00:00}' AND '{1:dd/MM/yyyy 23:59:59}'",
                    fechaIni, fechaFin);
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count<=0)
                {
                    ClFunciones.MensajeError("No se encontraron registros.");
                    return;
                }
                gcGastos.DataSource = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }
    }
}
