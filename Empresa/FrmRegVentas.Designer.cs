﻿namespace Empresa
{
    partial class FrmRegVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegVentas));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            this.gcProd = new DevExpress.XtraGrid.GridControl();
            this.gvProd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtFechaReg = new dll.Controles.ucLabelTextBox();
            this.txtValto = new dll.Controles.ucLabelTextBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtCantidad = new dll.Controles.ucLabelTextBox();
            this.dtpFechaEnvio = new DevExpress.XtraEditors.DateEdit();
            this.txtProd = new dll.Controles.ucBusqueda();
            this.txtValUnit = new dll.Controles.ucLabelTextBox();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEnvio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEnvio.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcProd
            // 
            this.gcProd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcProd.Location = new System.Drawing.Point(12, 131);
            this.gcProd.MainView = this.gvProd;
            this.gcProd.Name = "gcProd";
            this.gcProd.Size = new System.Drawing.Size(743, 296);
            this.gcProd.TabIndex = 7;
            this.gcProd.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvProd});
            // 
            // gvProd
            // 
            this.gvProd.GridControl = this.gcProd;
            this.gvProd.Name = "gvProd";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.tableLayoutPanel2);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(743, 113);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "Información de producto";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel2.Controls.Add(this.txtFechaReg, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtValto, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtCantidad, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.dtpFechaEnvio, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtProd, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtValUnit, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnAdd, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnLimpiar, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnSalir, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnGuardar, 5, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(5, 23);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(733, 85);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // txtFechaReg
            // 
            this.txtFechaReg.AnchoTitulo = 100;
            this.tableLayoutPanel2.SetColumnSpan(this.txtFechaReg, 3);
            this.txtFechaReg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFechaReg.Enabled = false;
            this.txtFechaReg.Location = new System.Drawing.Point(256, 3);
            this.txtFechaReg.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtFechaReg.MensajeDeAyuda = null;
            this.txtFechaReg.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtFechaReg.Name = "txtFechaReg";
            this.txtFechaReg.PermiteSoloNumeros = false;
            this.txtFechaReg.Size = new System.Drawing.Size(390, 24);
            this.txtFechaReg.TabIndex = 15;
            this.txtFechaReg.TextoTitulo = "Fecha registro :";
            this.txtFechaReg.ValorTextBox = "";
            // 
            // txtValto
            // 
            this.txtValto.AnchoTitulo = 100;
            this.tableLayoutPanel2.SetColumnSpan(this.txtValto, 2);
            this.txtValto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtValto.Enabled = false;
            this.txtValto.Location = new System.Drawing.Point(429, 57);
            this.txtValto.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtValto.MensajeDeAyuda = null;
            this.txtValto.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtValto.Name = "txtValto";
            this.txtValto.PermiteSoloNumeros = false;
            this.txtValto.Size = new System.Drawing.Size(217, 24);
            this.txtValto.TabIndex = 14;
            this.txtValto.TextoTitulo = "Valor total :";
            this.txtValto.ValorTextBox = "";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(74, 21);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Fecha venta :";
            // 
            // txtCantidad
            // 
            this.txtCantidad.AnchoTitulo = 100;
            this.txtCantidad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCantidad.Location = new System.Drawing.Point(256, 57);
            this.txtCantidad.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCantidad.MensajeDeAyuda = null;
            this.txtCantidad.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.PermiteSoloNumeros = false;
            this.txtCantidad.Size = new System.Drawing.Size(167, 24);
            this.txtCantidad.TabIndex = 3;
            this.txtCantidad.TextoTitulo = "Cant :";
            this.txtCantidad.ValorTextBox = "";
            this.txtCantidad.SaleControl += new dll.Controles.ucLabelTextBox.EventHandler(this.ucLabelTextBox1_SaleControl);
            // 
            // dtpFechaEnvio
            // 
            this.dtpFechaEnvio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpFechaEnvio.EditValue = new System.DateTime(((long)(0)));
            this.dtpFechaEnvio.Location = new System.Drawing.Point(83, 3);
            this.dtpFechaEnvio.Name = "dtpFechaEnvio";
            this.dtpFechaEnvio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.dtpFechaEnvio.Properties.Appearance.Options.UseFont = true;
            this.dtpFechaEnvio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFechaEnvio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFechaEnvio.Size = new System.Drawing.Size(167, 24);
            this.dtpFechaEnvio.TabIndex = 10;
            // 
            // txtProd
            // 
            this.txtProd.AnchoTextBox = 150;
            this.txtProd.AnchoTitulo = 80;
            this.tableLayoutPanel2.SetColumnSpan(this.txtProd, 4);
            this.txtProd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtProd.Location = new System.Drawing.Point(0, 27);
            this.txtProd.Margin = new System.Windows.Forms.Padding(0);
            this.txtProd.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtProd.MensajeDeAyuda = null;
            this.txtProd.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtProd.Name = "txtProd";
            this.txtProd.PermiteSoloNumeros = false;
            this.txtProd.Size = new System.Drawing.Size(599, 28);
            this.txtProd.TabIndex = 1;
            this.txtProd.TextoTitulo = "Producto :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtProd.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtProd.ValorTextBox = "";
            this.txtProd.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtProd_SaleControl);
            // 
            // txtValUnit
            // 
            this.txtValUnit.AnchoTitulo = 77;
            this.tableLayoutPanel2.SetColumnSpan(this.txtValUnit, 2);
            this.txtValUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtValUnit.Location = new System.Drawing.Point(3, 57);
            this.txtValUnit.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtValUnit.MensajeDeAyuda = null;
            this.txtValUnit.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtValUnit.Name = "txtValUnit";
            this.txtValUnit.PermiteSoloNumeros = false;
            this.txtValUnit.Size = new System.Drawing.Size(247, 24);
            this.txtValUnit.TabIndex = 2;
            this.txtValUnit.TextoTitulo = "Valor unit :";
            this.txtValUnit.ValorTextBox = "";
            this.txtValUnit.SaleControl += new dll.Controles.ucLabelTextBox.EventHandler(this.txtValUnit_SaleControl);
            // 
            // btnAdd
            // 
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(602, 30);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(44, 21);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(655, 30);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 21);
            this.btnLimpiar.TabIndex = 6;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(655, 57);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(655, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 21);
            this.btnGuardar.TabIndex = 5;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // FrmRegVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 439);
            this.Controls.Add(this.gcProd);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmRegVentas";
            this.Text = "Registro de Ventas";
            this.Load += new System.EventHandler(this.FrmRegVentas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEnvio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEnvio.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcProd;
        private DevExpress.XtraGrid.Views.Grid.GridView gvProd;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.DateEdit dtpFechaEnvio;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private dll.Controles.ucLabelTextBox txtValUnit;
        private dll.Controles.ucBusqueda txtProd;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private dll.Controles.ucLabelTextBox txtCantidad;
        private dll.Controles.ucLabelTextBox txtValto;
        private dll.Controles.ucLabelTextBox txtFechaReg;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}