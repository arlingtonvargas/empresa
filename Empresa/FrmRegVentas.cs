﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmRegVentas : Form
    {
        DataTable dtDetalle = new DataTable();
        public FrmRegVentas()
        {
            InitializeComponent();
        }

        private void FrmRegVentas_Load(object sender, EventArgs e)
        {
            txtFechaReg.ValorTextBox = DateTime.Now.ToString("ddd MM yyyy hh:mm tt");
            CrearTabla();
            CrearGrilla();
            LlenarControlProds();
            txtProd.Focus();
            dtpFechaEnvio.DateTime = DateTime.Now;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            VALIDAR QUE NO SUPERE LOS EXISTENTES
            if (dtpFechaEnvio.EditValue != null && txtProd.ValorTextBox != "" && txtValUnit.ValorTextBox !="" && txtCantidad.ValorTextBox!="" && txtValto.ValorTextBox !="")
            {
                DataRow fila = dtDetalle.NewRow();                
                fila["IdProd"] = txtProd.ValorTextBox;
                fila["Nombre"] = txtProd.lblDescripcion.Text;
                fila["Cantidad"] = txtCantidad.ValorTextBox;
                fila["ValUni"] = txtValUnit.ValorTextBox;
                fila["Valto"] = Convert.ToInt32(txtValUnit.ValorTextBox) * Convert.ToInt32(txtCantidad.ValorTextBox);
                dtDetalle.Rows.Add(fila);
                dtDetalle.AcceptChanges();
                gcProd.DataSource = dtDetalle;
                LimpiarCampos();
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvProd.RowCount > 0)
                {
                    DataTable dt = (DataTable)gcProd.DataSource;
                    if (GuardarVenta(dt))
                    {
                        ClFunciones.MensajeExitoso("Venta registrada correctamente.");
                        dtDetalle.Rows.Clear();
                        LimpiarCampos();
                    }
                    else
                    {
                        ClFunciones.MensajeError("Lo sentimos, ha ocurrido un error.");
                    }
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }            
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
            LlenarControlProds();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void CrearTabla()
        {
            dtDetalle.Columns.Add("IdVentaDetalle", typeof(int));
            dtDetalle.Columns.Add("IdVenta", typeof(int));
            dtDetalle.Columns.Add("IdProd", typeof(int));
            dtDetalle.Columns.Add("Nombre", typeof(string));
            dtDetalle.Columns.Add("Cantidad", typeof(int));
            dtDetalle.Columns.Add("ValUni", typeof(int));
            dtDetalle.Columns.Add("Valto", typeof(string));
        }
        private void LlenarControlProds()
        {
            try
            {
                string sql = "SELECT Sec AS Codigo, Nombre AS Descripcion FROM Productos";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                txtProd.DataTable = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }



        private void CrearGrilla()
        {
            try
            {
                gvProd = GrillaDevExpress.CrearGrilla(false, false, "");
                gvProd.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvProd.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("IdVentaDetalle", "", visible: false));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("IdVenta", "", visible: false));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("IdProd", "Id prod", ancho: 50));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Nombre", "Producto", ancho: 150));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Cantidad", "Cantidad", ancho: 70));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("ValUni", "Valor unitario",  ancho: 70));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Valto", "Total", ancho: 70));
                gvProd.OptionsBehavior.Editable = true;
                gvProd.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvProd.OptionsCustomization.AllowColumnResizing = true;
                gvProd.OptionsView.ShowFooter = true;
                gvProd.Columns["Valto"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                gvProd.Columns["Valto"].SummaryItem.FieldName = "Valto";
                gvProd.Columns["Valto"].SummaryItem.DisplayFormat = "Total : {0:C2}";
                gcProd.MainView = gvProd;
                //this.gvProd.DoubleClick += new System.EventHandler(this.gvProd_DoubleClick);
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private bool GuardarVenta(DataTable dt)
        {
            try
            {
                 string sql = "";
                 using (TransactionScope scope = new TransactionScope())
                 {
                     using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                     {
                         con.Open();
                         int Sec = ClFunciones.TraeSiguienteSecuencial("Ventas", "Sec", con);
                         if (Sec > 0)
                         {
                             sql = string.Format("set dateformat dmy; INSERT INTO VENTAS (Sec, FechaVenta, FechaRegistro) VALUES ({0}, '{1:dd/MM/yyyy HH:mm:ss}', '{2:dd/MM/yyyy HH:mm:ss}')",
                                 Sec, dtpFechaEnvio.EditValue, DateTime.Now);
                             string res = ClFunciones.EjecutaComando(sql, con);
                             if (res=="si")
                             {
                                int _Sec = ClFunciones.TraeSiguienteSecuencial("VentaDetalle", "IdVentaDetalle", con);
                                _Sec -= 1;
                                foreach (DataRow item in dt.Rows)
                                 {
                                    _Sec += 1;
                                    sql = string.Format("INSERT INTO VENTADETALLE (IdVentaDetalle, IdVenta, IdProd, Cantidad, ValUni, ValTo)" +
                                        " VALUES ({0}, {1}, {2}, {3}, {4}, {5})", _Sec, Sec, item["IdProd"], item["Cantidad"], item["ValUni"], item["ValTo"]);
                                     res = ClFunciones.EjecutaComando(sql, con);
                                     if (res!="si")
                                     {
                                         return false;
                                     }
                                    sql = string.Format("UPDATE Productos SET Cantidad = Cantidad - {0} WHERE Sec = {1}",
                                    item["Cantidad"], item["IdProd"]);
                                    res = ClFunciones.EjecutaComando(sql, con);
                                    if (res != "si")
                                    {
                                        return false;
                                    }
                                }
                             }
                         }
                         scope.Complete();                         
                     }
                 }
                return true;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
                return false;
            }
        }


        private void LimpiarCampos()
        {
            txtFechaReg.ValorTextBox = DateTime.Now.ToString("ddd MM yyyy hh:mm tt");
            txtProd.ValorTextBox = "";
            txtValUnit.ValorTextBox = "";
            txtCantidad.ValorTextBox = "";
            txtValto.ValorTextBox = "";
            txtProd.Focus();
        }

        private void txtValUnit_SaleControl(object sender, EventArgs e)
        {
            try
            {
                if (txtValUnit.ValorTextBox != "" && txtCantidad.ValorTextBox != "")
                {
                    txtValto.ValorTextBox = (Convert.ToInt32(txtValUnit.ValorTextBox) * Convert.ToInt32(txtCantidad.ValorTextBox)).ToString(); 
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void ucLabelTextBox1_SaleControl(object sender, EventArgs e)
        {
            try
            {
                if (txtCantidad.ValorTextBox != "" && txtValUnit.ValorTextBox != "")
                {
                    txtValto.ValorTextBox = (Convert.ToInt32(txtValUnit.ValorTextBox) * Convert.ToInt32(txtCantidad.ValorTextBox)).ToString();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void txtProd_SaleControl(object sender, EventArgs e)
        {
            try
            {
                if (txtProd.ValorTextBox !="")
                {
                    string sql = "SELECT PrecioVenta FROM PRODUCTOS WHERE SEC = " + txtProd.ValorTextBox;
                    DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    if (dt.Rows.Count>0)
                    {
                        txtValUnit.ValorTextBox = dt.Rows[0][0].ToString();
                    }
                }
                else
                {
                    txtValUnit.ValorTextBox = "";
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }
    }
}
