﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Empresa.ET;

namespace Empresa
{
    public partial class FrmListaSeparados : DevExpress.XtraEditors.XtraForm
    {
        SeparadosListaVistaET ListaSeparados = new SeparadosListaVistaET();
        public FrmListaSeparados()
        {
            InitializeComponent();
        }

        private void FrmListaSeparados_Load(object sender, EventArgs e)
        {
            separadosListaVistaETBindingSource.DataSource = ListaSeparados;
            dtpFechaIni.DateTime = new DateTime(DateTime.Now.Year, 1, 1);
            dtpFechaFin.DateTime = new DateTime(DateTime.Now.Year, 12, 31);
        }

        private void LlenaGrilla()
        {
            try
            {
                ListaSeparados.Clear();
                List<ParametersSpET> listParam = new List<ParametersSpET>();
                listParam.Add(new ParametersSpET() { NombreParametro = "@pFechaIni", Valor = dtpFechaIni.DateTime });
                listParam.Add(new ParametersSpET() { NombreParametro = "@pFecfaFin", Valor = dtpFechaFin.DateTime });
                ListaSeparados.AddRange(ClFunciones.DataReaderMapToListConSP<SeparadosVistaET>("SP_SeparadosList_G", listParam).ToList());
                gcSeparados.RefreshDataSource();
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
            
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            if (dtpFechaIni.DateTime == null || dtpFechaFin.DateTime == null) return;
            LlenaGrilla();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            List<ParametersSpET> listParam = new List<ParametersSpET>();
            SeparadosDetalleVistaListET separadoDet = new SeparadosDetalleVistaListET();
            AbonosSeparadoListET Abonos = new AbonosSeparadoListET();
            SeparadosVistaET separado = (SeparadosVistaET)separadosListaVistaETBindingSource.Current;
            listParam.Add(new ParametersSpET() { NombreParametro = "@pSecSeparado", Valor = separado.Sec });
            separadoDet.AddRange(ClFunciones.DataReaderMapToListConSP<SeparadosDetalleVistaET>("SP_SeparadosDetalleByIdSeparado_G", listParam).ToList());
            listParam = new List<ParametersSpET>();
            listParam.Add(new ParametersSpET() { NombreParametro = "@pIdSeparado", Valor = separado.Sec });
            Abonos.AddRange(ClFunciones.DataReaderMapToListConSP<AbonosSeparadoET>("SP_AbonosSeparadoByIdSeparado_G", listParam).ToList());
            FrmSeparado frm = new FrmSeparado(false, separadoDet, Abonos, separado);
            frm.ShowDialog();
        }
    }
}