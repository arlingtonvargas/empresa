﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.ET
{
   public  class SeparadosET
    {
        public int Sec { get; set; }
        public DateTime FechaSeparo { get; set; }
        public DateTime FechaFinSeparo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public long CelCliente { get; set; }
    }
}
