﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.ET
{
    public class AbonosSeparadoET
    {
        public int Sec { get; set; }
        public int IdSeparado { get; set; }
        public DateTime FechaAbono { get; set; }
        public decimal ValAbono { get; set; }        
    }

    public class AbonosSeparadoListET : List<AbonosSeparadoET> { }
}
