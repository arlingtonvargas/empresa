﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.ET
{
    public class ParametersSpET
    {
        public string NombreParametro { get; set; }
        public object Valor { get; set; }
    }

    public class ParametersSpETList : List<ParametersSpET> { }
}
