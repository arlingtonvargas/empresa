﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.ET
{
    public class SeparadosVistaET : SeparadosET
    {
        public decimal ValSeparado { get; set; }
        public decimal ValAbonado { get; set; }
    }
    public class SeparadosListaVistaET : List<SeparadosVistaET>
    {
    }
}
