﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.ET
{
    public class SeparadosDetalleET
    {
        public int Sec { get; set; }
        public int SecSeparado { get; set; }
        public int IdProd { get; set; }
        public decimal ValProd { get; set; }
        public int Cant { get; set; }
        public decimal ValTo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }

    public class SeparadosDetalleVistaET : SeparadosDetalleET
    {
        public string NomProd { get; set; }
    }

    public class SeparadosDetalleVistaListET : List<SeparadosDetalleVistaET> { }
}
