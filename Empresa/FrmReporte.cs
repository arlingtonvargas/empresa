﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmReporte : Form
    {
        public FrmReporte()
        {
            InitializeComponent();
        }

        private void FrmReporte_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenarGrilla();
        }

        private void LlenarGrilla()
        {
            gridControl1.DataSource = ClFunciones.ConsultarConSP("SP_ProductosReport_G", ClConexion.clConexion.Conexion);
        }


        private void CrearGrilla()
        {
            try
            {
                gridView1 = GrillaDevExpress.CrearGrilla(false, false, "");
                gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gridView1.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Id", ancho: 20));
                gridView1.Columns.Add(GrillaDevExpress.CrearColumna("Nombre", "Nombre"));
                gridView1.Columns.Add(GrillaDevExpress.CrearColumna("Descripcion", "Descripción", ancho: 120));
                gridView1.Columns.Add(GrillaDevExpress.CrearColumna("Existencia", "Existencia", ancho: 50));
                gridView1.Columns.Add(GrillaDevExpress.CrearColumna("PrecioCosto", "P. costo", tipo: DevExpress.Utils.FormatType.Numeric, formato: "C2", ancho: 50));
                gridView1.Columns.Add(GrillaDevExpress.CrearColumna("TotalCosto", "Total. costo", tipo: DevExpress.Utils.FormatType.Numeric, formato: "C2", ancho: 50));
                gridView1.Columns.Add(GrillaDevExpress.CrearColumna("PrecioVenta", "P. venta", tipo: DevExpress.Utils.FormatType.Numeric, formato: "C2", ancho: 50));
                gridView1.Columns.Add(GrillaDevExpress.CrearColumna("TotalVenta", "Total. venta", tipo: DevExpress.Utils.FormatType.Numeric, formato: "C2", ancho: 50));               
                gridView1.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gridView1.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gridView1.OptionsView.ShowFooter = true;
                gridView1.Columns["TotalCosto"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                gridView1.Columns["TotalCosto"].SummaryItem.FieldName = "TotalCosto";
                gridView1.Columns["TotalCosto"].SummaryItem.DisplayFormat = "Total {0:C2}";
                gridView1.Columns["TotalVenta"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                gridView1.Columns["TotalVenta"].SummaryItem.FieldName = "TotalVenta";
                gridView1.Columns["TotalVenta"].SummaryItem.DisplayFormat = "Total {0:C2}";
                gridView1.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gridView1.OptionsCustomization.AllowColumnResizing = true;
                gridControl1.MainView = gridView1;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenarGrilla();
        }
    }
}
