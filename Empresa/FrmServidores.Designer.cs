﻿namespace Colegio.Vistas
{
    partial class FrmServidores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmServidores));
            this.btnEliminaServ = new System.Windows.Forms.Button();
            this.btnAddServ = new System.Windows.Forms.Button();
            this.txtServ = new System.Windows.Forms.TextBox();
            this.lbxServ = new System.Windows.Forms.ListBox();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblCerrar = new DevExpress.XtraEditors.LabelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btnEliminaServ
            // 
            this.btnEliminaServ.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminaServ.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminaServ.Image")));
            this.btnEliminaServ.Location = new System.Drawing.Point(368, 49);
            this.btnEliminaServ.Name = "btnEliminaServ";
            this.btnEliminaServ.Size = new System.Drawing.Size(34, 27);
            this.btnEliminaServ.TabIndex = 22;
            this.btnEliminaServ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminaServ.UseVisualStyleBackColor = true;
            this.btnEliminaServ.Click += new System.EventHandler(this.btnEliminaServ_Click);
            // 
            // btnAddServ
            // 
            this.btnAddServ.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddServ.Image = ((System.Drawing.Image)(resources.GetObject("btnAddServ.Image")));
            this.btnAddServ.Location = new System.Drawing.Point(335, 49);
            this.btnAddServ.Name = "btnAddServ";
            this.btnAddServ.Size = new System.Drawing.Size(34, 27);
            this.btnAddServ.TabIndex = 21;
            this.btnAddServ.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddServ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddServ.UseVisualStyleBackColor = true;
            this.btnAddServ.Click += new System.EventHandler(this.btnAddServ_Click);
            // 
            // txtServ
            // 
            this.txtServ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtServ.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServ.Location = new System.Drawing.Point(20, 50);
            this.txtServ.Name = "txtServ";
            this.txtServ.Size = new System.Drawing.Size(315, 25);
            this.txtServ.TabIndex = 20;
            // 
            // lbxServ
            // 
            this.lbxServ.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxServ.FormattingEnabled = true;
            this.lbxServ.ItemHeight = 17;
            this.lbxServ.Location = new System.Drawing.Point(20, 81);
            this.lbxServ.Name = "lbxServ";
            this.lbxServ.Size = new System.Drawing.Size(382, 225);
            this.lbxServ.TabIndex = 24;
            this.lbxServ.Click += new System.EventHandler(this.lbxServ_Click);
            this.lbxServ.DoubleClick += new System.EventHandler(this.lbxServ_DoubleClick);
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(0, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblTitulo.Size = new System.Drawing.Size(422, 32);
            this.lblTitulo.TabIndex = 25;
            this.lblTitulo.Text = "Lista Servidores";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCerrar
            // 
            this.lblCerrar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblCerrar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCerrar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCerrar.Appearance.Options.UseBackColor = true;
            this.lblCerrar.Appearance.Options.UseFont = true;
            this.lblCerrar.Appearance.Options.UseForeColor = true;
            this.lblCerrar.Appearance.Options.UseTextOptions = true;
            this.lblCerrar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblCerrar.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCerrar.Location = new System.Drawing.Point(390, 3);
            this.lblCerrar.Name = "lblCerrar";
            this.lblCerrar.Size = new System.Drawing.Size(30, 25);
            this.lblCerrar.TabIndex = 26;
            this.lblCerrar.Text = "X";
            this.lblCerrar.Click += new System.EventHandler(this.lblCerrar_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(422, 298);
            this.panel1.TabIndex = 27;
            // 
            // FrmServidores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 329);
            this.Controls.Add(this.lblCerrar);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lbxServ);
            this.Controls.Add(this.btnEliminaServ);
            this.Controls.Add(this.btnAddServ);
            this.Controls.Add(this.txtServ);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(422, 329);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(422, 329);
            this.Name = "FrmServidores";
            this.Text = "Lista Servidores";
            this.Load += new System.EventHandler(this.FrmServidores_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnEliminaServ;
        private System.Windows.Forms.Button btnAddServ;
        private System.Windows.Forms.TextBox txtServ;
        private System.Windows.Forms.ListBox lbxServ;
        public System.Windows.Forms.Label lblTitulo;
        private DevExpress.XtraEditors.LabelControl lblCerrar;
        private System.Windows.Forms.Panel panel1;
    }
}