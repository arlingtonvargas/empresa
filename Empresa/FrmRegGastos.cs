﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmRegGastos : Form
    {
        DataTable dtGastos = new DataTable();
        public FrmRegGastos()
        {
            InitializeComponent();
        }

        private void FrmRegGastos_Load(object sender, EventArgs e)
        {
            dtGastos.Columns.Add("IdGasto", typeof(int));
            dtGastos.Columns.Add("Descripcion", typeof(string));
            dtGastos.Columns.Add("Fecha", typeof(DateTime));
            dtGastos.Columns.Add("valor", typeof(double));
            gcGastos.DataSource = dtGastos;
            gvGastos.Columns["valor"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gvGastos.Columns["valor"].SummaryItem.FieldName = "valor";
            gvGastos.Columns["valor"].SummaryItem.DisplayFormat = "Total : {0:C2}";
            dtpFecha.DateTime = DateTime.Now;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtDesGasto.ValorTextBox != "" && dtpFecha.EditValue != null && txtVal.ValorTextBox != "")
            {
                DataRow fila = dtGastos.NewRow();
                fila["Descripcion"] = txtDesGasto.ValorTextBox; 
                fila["Fecha"] = dtpFecha.EditValue;
                fila["valor"] = txtVal.ValorTextBox;
                dtGastos.Rows.Add(fila);
                dtGastos.AcceptChanges();
                gcGastos.DataSource = dtGastos;
                txtDesGasto.ValorTextBox = "";
                txtVal.ValorTextBox = "";
                txtDesGasto.Focus();
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvGastos.RowCount>0)
                {
                    DataTable dt = (DataTable)gcGastos.DataSource;
                    if (GuardarGastos(dt))
                    {
                        ClFunciones.MensajeExitoso(string.Format("Gastos por {0:C2} registrados correctamente.",dt.Compute("sum(Valor)", "")));
                        LimpiarCampos();
                    }
                    else
                    {
                        ClFunciones.MensajeExitoso("Lo sentimos, ha ocurrido un error.");
                    }
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LimpiarCampos()
        {
            txtDesGasto.ValorTextBox = "";
            txtVal.ValorTextBox = "";
            dtpFecha.EditValue = DateTime.Now;
            dtGastos.Rows.Clear();
            gcGastos.DataSource = dtGastos;
            txtDesGasto.Focus();
        }

        private bool GuardarGastos(DataTable dt)
        {
            try
            {
                string sql = "";
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        int Sec = ClFunciones.TraeSiguienteSecuencial("Gastos", "IdGasto", con);
                        if (Sec > 0)
                        {
                            int _sec = Sec - 1;
                            foreach (DataRow item in dt.Rows)
                            {
                                _sec += 1;
                                sql = string.Format("INSERT INTO [dbo].[Gastos] ([IdGasto] ,[Descripcion] ,[Fecha] ,[valor]) " +
                                " VALUES({0}  ,'{1}', '{2:dd/MM/yyyy HH:mm:ss}' ,{3})", _sec, item["Descripcion"], item["Fecha"], item["valor"]);
                                string res = ClFunciones.EjecutaComando(sql, con);
                                if (res != "si")
                                {
                                    return false;
                                }
                            }
                        }
                        scope.Complete();
                    }
                }
                             
                return true;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
                return false;
            }
        }
    }
}
