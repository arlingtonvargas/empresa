﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";

        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            dll.Common.Class.ClFuncionesdll.ColorFondoControles = Color.FromArgb(241, 243, 244);
            this.BackColor = Color.FromArgb(255, 195, 200);
            SetSkin();
            lblVersion.Text = "El Baul de ELi App V" + ClFunciones.VersionApp;
            txtContra.txtCampo.Properties.PasswordChar = '*';
            txtUsu.Focus();
        }
        public void SetSkin()
        {
            //DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "Office 2016 Colorful";
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (txtUsu.ValorTextBox !="" && txtContra.ValorTextBox != "")
            {
                if (ClConexion.clConexion.Ingreso())
                {
                    string sql = string.Format("SELECT * FROM USUARIOS WHERE NombreUsu = '{0}' AND Password = '{1}'",txtUsu.ValorTextBox, txtContra.ValorTextBox);
                    DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    if (dt.Rows.Count>0)
                    {
                        this.Hide();
                        FrmPrincipal frm = new FrmPrincipal(this);
                        frm.ShowDialog();
                    }
                    else
                    {
                        ClFunciones.MensajeError("Usuario o contraseña incorrectos.");
                    }                    
                }
            }
            
        }
    }
}
