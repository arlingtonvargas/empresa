﻿namespace Empresa
{
    partial class FrmRegGastos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegGastos));
            this.gcGastos = new DevExpress.XtraGrid.GridControl();
            this.gvGastos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtVal = new dll.Controles.ucLabelTextBox();
            this.dtpFecha = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtCant = new dll.Controles.ucLabelTextBox();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.txtDesGasto = new dll.Controles.ucLabelTextBox();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcGastos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGastos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcGastos
            // 
            this.gcGastos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcGastos.Location = new System.Drawing.Point(12, 102);
            this.gcGastos.MainView = this.gvGastos;
            this.gcGastos.Name = "gcGastos";
            this.gcGastos.Size = new System.Drawing.Size(779, 301);
            this.gcGastos.TabIndex = 7;
            this.gcGastos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvGastos});
            // 
            // gvGastos
            // 
            this.gvGastos.GridControl = this.gcGastos;
            this.gvGastos.Name = "gvGastos";
            this.gvGastos.OptionsBehavior.Editable = false;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.txtVal);
            this.groupControl1.Controls.Add(this.dtpFecha);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.txtCant);
            this.groupControl1.Controls.Add(this.btnAdd);
            this.groupControl1.Controls.Add(this.txtDesGasto);
            this.groupControl1.Controls.Add(this.btnLimpiar);
            this.groupControl1.Controls.Add(this.btnGuardar);
            this.groupControl1.Controls.Add(this.btnSalir);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(779, 84);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "Información del gasto";
            // 
            // txtVal
            // 
            this.txtVal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVal.AnchoTitulo = 80;
            this.txtVal.Location = new System.Drawing.Point(257, 54);
            this.txtVal.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtVal.MensajeDeAyuda = null;
            this.txtVal.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtVal.Name = "txtVal";
            this.txtVal.PermiteSoloNumeros = true;
            this.txtVal.Size = new System.Drawing.Size(313, 24);
            this.txtVal.TabIndex = 3;
            this.txtVal.TextoTitulo = "Valor :";
            this.txtVal.ValorTextBox = "";
            // 
            // dtpFecha
            // 
            this.dtpFecha.EditValue = new System.DateTime(((long)(0)));
            this.dtpFecha.Location = new System.Drawing.Point(107, 55);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.dtpFecha.Properties.Appearance.Options.UseFont = true;
            this.dtpFecha.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFecha.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFecha.Size = new System.Drawing.Size(144, 24);
            this.dtpFecha.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(27, 55);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(74, 23);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Fecha gasto :";
            // 
            // txtCant
            // 
            this.txtCant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCant.AnchoTitulo = 50;
            this.txtCant.Location = new System.Drawing.Point(1952, 57);
            this.txtCant.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCant.MensajeDeAyuda = null;
            this.txtCant.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCant.Name = "txtCant";
            this.txtCant.PermiteSoloNumeros = true;
            this.txtCant.Size = new System.Drawing.Size(144, 24);
            this.txtCant.TabIndex = 11;
            this.txtCant.TextoTitulo = "Cant :";
            this.txtCant.ValorTextBox = "";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(576, 55);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(38, 22);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtDesGasto
            // 
            this.txtDesGasto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDesGasto.AnchoTitulo = 100;
            this.txtDesGasto.Location = new System.Drawing.Point(5, 25);
            this.txtDesGasto.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDesGasto.MensajeDeAyuda = null;
            this.txtDesGasto.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDesGasto.Name = "txtDesGasto";
            this.txtDesGasto.PermiteSoloNumeros = false;
            this.txtDesGasto.Size = new System.Drawing.Size(609, 24);
            this.txtDesGasto.TabIndex = 1;
            this.txtDesGasto.TextoTitulo = "Descripción :";
            this.txtDesGasto.ValorTextBox = "";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(699, 25);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 6;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(620, 25);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 53);
            this.btnGuardar.TabIndex = 5;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(699, 55);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // FrmRegGastos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 415);
            this.Controls.Add(this.gcGastos);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmRegGastos";
            this.Text = "Registro Gastos";
            this.Load += new System.EventHandler(this.FrmRegGastos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcGastos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGastos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcGastos;
        private DevExpress.XtraGrid.Views.Grid.GridView gvGastos;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private dll.Controles.ucLabelTextBox txtCant;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.DateEdit dtpFecha;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private dll.Controles.ucLabelTextBox txtDesGasto;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private dll.Controles.ucLabelTextBox txtVal;
    }
}