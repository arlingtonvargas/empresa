﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;
using DevExpress.XtraEditors;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Collections;
using System.Reflection;
using Empresa.ET;

namespace Empresa
{
    class ClFunciones
    {
        public static ClFunciones f = new ClFunciones();
        public static FrmPrincipal formPrinci = new FrmPrincipal(new FrmLogin());
        public static string NomUsu;
        public static List<string> listaServidores = new List<string>();
        public static string VersionApp = Application.ProductVersion;

        public enum EnumImagenes16X16
        {
            Conectar,
            Desconectar,
            Salir,
            Maestros,

        };
        //public static Image Imagen_boton16X16(EnumImagenes16X16 NumImg)
        //{
        //    FrmPrincipal frm = new FrmPrincipal();
        //    //Image img = frm.Imagenes16X16.Images[Convert.ToInt16(NumImg)];
        //    return img;
        //}

        //public enum EnumImagenes32X32
        //{
        //    Conectar,
        //    Desconectar,

        //};

        //public static Image Imagen_boton32X32(EnumImagenes32X32 NumImg)
        //{
        //    FrmPrincipal frm = new FrmPrincipal();
        //    //Image img = frm.Imagenes32X32.Images[Convert.ToInt16(NumImg)];
        //    return img;
        //} 
        
        public static DialogResult MensajeExitoso(string Mensaje, string Caption= "Mensaje | El Baul de ELi")
        {
            return MessageBox.Show(Mensaje, Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);            
        }

        public static DialogResult MensajeError(string Mensaje, string Caption= "Mensaje de error")
        {
            return MessageBox.Show(Mensaje, Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public DataTable AbrirTabla(SqlConnection Con, string SQL)
        {
            try
            {
                SqlCommand Cmd = new SqlCommand();
                DataTable TB = new DataTable();
                SqlDataAdapter Adaptador = default(SqlDataAdapter);
                Cmd.Connection = Con;
                Cmd.CommandText = SQL;
                Cmd.CommandType = CommandType.Text;
                Adaptador = new SqlDataAdapter(Cmd);
                Adaptador.Fill(TB);
                Adaptador.Dispose();
                Cmd.Dispose();
                return TB;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
                return null;
            }

        }
        public void AvanzaConEnter(KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
        //public static void msgError(string Mensaje, string Titulo = "")
        //{
        //    MessageBox.Show(Mensaje, Convert.ToString((!string.IsNullOrEmpty(Titulo) ? Titulo : "Mensaje del Sistema")), MessageBoxButtons.OK, MessageBoxIcon.Error);
        //}
        //public static void msgError(string Mensaje, string Titulo = "", bool MensajeCentrado = false)
        //{
        //    //FrmMensaje frmMsj = new FrmMensaje();
        //    //frmMsj.lblTitulo.BackColor = Color.FromArgb(216, 99, 68);
        //    //if (MensajeCentrado) { frmMsj.lblMensaje.TextAlign = ContentAlignment.MiddleCenter; }
        //    //frmMsj.lblMensaje.Text = Mensaje;
        //    //frmMsj.ShowDialog();
        //}
        //public static void msgExitoso(string Mensaje, string Titulo = "", bool MensajeCentrado = false)
        //{
        //    //FrmMensaje frmMsj = new FrmMensaje();
        //    //if (MensajeCentrado) { frmMsj.lblMensaje.TextAlign = ContentAlignment.MiddleCenter; }
        //    //frmMsj.lblMensaje.Text = Mensaje;
        //    //frmMsj.ShowDialog();
        //}

        //public static DialogResult msgResult(string Mensaje, MessageBoxButtons Botones, MessageBoxIcon Icono, string Titulo = "")
        //{
        //    return MessageBox.Show(Mensaje, Convert.ToString((!string.IsNullOrEmpty(Titulo) ? Titulo : "Mensaje del Sistema")), Botones, Icono);
        //}

        public static void ValidaMdiParent()
        {
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                Form form = Application.OpenForms[i];
                if (form.GetType() == typeof(FrmPrincipal) && i== Application.OpenForms.Count-1)
                {
                    formPrinci.IsMdiContainer = false;
                    return;
                }
            }
           
        }

        public static void AgregaServidorALista(string[] lista)
        {
            try
            {
                const string userRoot = "HKEY_CURRENT_USER";
                const string subkey = "GuiAcceso";
                const string keyName = userRoot + "\\" + subkey;
                Registry.SetValue(keyName, "Servidores", lista);              
            }
            catch (Exception ex)
            {
                MensajeError(ex.Message);
            }
        }

        public static string[] ObtenerListaDeServidores()
        {
            try
            {
                const string userRoot = "HKEY_CURRENT_USER";
                const string subkey = "GuiAcceso";
                const string keyName = userRoot + "\\" + subkey;
                string[] tArray = (string[])Registry.GetValue(keyName,"Servidores", new string[] { "192.168.1.1\\RAPIENTREGA" });
                listaServidores.Clear();
                for (int i = 0; i < tArray.Length; i++)
                {
                    listaServidores.Add(tArray[i]);
                    //Console.WriteLine("Servidores({0}): {1}", i, tArray[i]);
                }
                return tArray;
            }
            catch (Exception ex)
            {
                MensajeError(ex.Message);
                return null;
            }
        }

        public static object DataReaderMapToObjectConSP<T>(string SPname, List<ParametersSpET> sqlParameters = null)
        {
            T obj = default(T);
            try
            {
                using (SqlConnection conn = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                {
                    SqlCommand cmd = new SqlCommand(SPname, conn);
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (sqlParameters != null) sqlParameters.ForEach(x => { cmd.Parameters.AddWithValue(x.NombreParametro, x.Valor); });
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {
                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    prop.SetValue(obj, dr[prop.Name], null);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ClRegistraLogs.RegistrarError(new ET.SGLogs()
                //{
                //    FechaReg = DateTime.Now,
                //    MensajeError = ex.Message,
                //    NomArchivo = "Funciones",
                //    NomError = "Exception",
                //    NomFunction = "DataReaderMapToList",
                //    NomModulo = "General",
                //    NumLinea = 325,
                //    Sec = 0
                //});
                return null;
            }
            return obj;
        }


        public static List<T> DataReaderMapToListConSP<T>(string SPname, List<ParametersSpET> sqlParameters = null)
        {
            List<T> list = new List<T>();
            try
            {
                using (SqlConnection conn = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                {
                    SqlCommand cmd = new SqlCommand(SPname, conn);
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (sqlParameters != null) sqlParameters.ForEach(x => { cmd.Parameters.AddWithValue(x.NombreParametro, x.Valor); });
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        T obj = default(T);
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {
                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    prop.SetValue(obj, dr[prop.Name], null);
                                }
                            }
                            list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ClRegistraLogs.RegistrarError(new ET.SGLogs()
                //{
                //    FechaReg = DateTime.Now,
                //    MensajeError = ex.Message,
                //    NomArchivo = "Funciones",
                //    NomError = "Exception",
                //    NomFunction = "DataReaderMapToList",
                //    NomModulo = "General",
                //    NumLinea = 325,
                //    Sec = 0
                //});
                return null;
            }

            return list;
        }



        public static void EntraControlDateEditDEV(DateEdit dte, LabelControl lbl = null, string msg = "")
        {
            dte.BackColor = Color.Cyan;
            dte.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            dte.Properties.Appearance.BorderColor = Color.Black;
            if ((lbl == null) == false)
            {
                lbl.Font = new Font("Segoe UI", lbl.Font.Size, System.Drawing.FontStyle.Bold);
            }
            //FrmPrincipal.frmPrinc.msgAyuda.Caption = msg;

        }
        public static void SaleControlDateEditDEV(DateEdit dte, LabelControl lbl = null)
        {
            dte.BackColor = Color.White;
            dte.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            dte.Properties.Appearance.BorderColor = Color.White;
            if ((lbl == null) == false)
            {
                lbl.Font = new Font("Segoe UI",lbl.Font.Size, System.Drawing.FontStyle.Regular);
            }
            //FrmPrincipal.frmPrinc.msgAyuda.Caption = "";
        }

        public static void EntraControlTxt(TextBox txt, Label lbl = null, string msg = "")
        {
            if (txt != null)
            {
                txt.BackColor = Color.LightCyan;
                txt.SelectionStart = 0;
                txt.SelectionLength = txt.Text.Length;
                //Txt.BorderStyle = BorderStyle.FixedSingle;
                txt.Font = new Font("Segoe UI", txt.Font.Size, System.Drawing.FontStyle.Bold);
            }

            if (lbl != null)
            {
                lbl.Font = new Font("Segoe UI", lbl.Font.Size, System.Drawing.FontStyle.Bold);
            }
            //FrmPrincipal.frmPrinc.msgAyuda.Caption = msg;

        }
        public static void SaleControlTxt( TextBox txt, Label lbl = null)
        {
            if (txt != null)
            {
                txt.BackColor = Color.White;
                txt.Font = new Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular);
                //Txt.BorderStyle = BorderStyle.FixedSingle;
            }
            if (lbl != null)
            {
                lbl.Font = new Font("Segoe UI", lbl.Font.Size, System.Drawing.FontStyle.Regular);
            }
            //FrmPrincipal.frmPrinc.msgAyuda.Caption = "";
        }

        public static void EntraControlCbx(System.Windows.Forms.ComboBox cbx, Label lbl = null, string msg = "")
        {
            if (cbx != null)
            {
                cbx.BackColor = Color.LightCyan;
                cbx.SelectAll();
                cbx.Font = new Font("Segoe UI", cbx.Font.Size, System.Drawing.FontStyle.Bold);
                cbx.Show();                         
            }

            if (lbl != null)
            {
                lbl.Font = new Font("Segoe UI", lbl.Font.Size, System.Drawing.FontStyle.Bold);
            }
            //FrmPrincipal.frmPrinc.msgAyuda.Caption = msg;

        }
        public static void SaleControlcbx(System.Windows.Forms.ComboBox cbx, Label lbl = null)
        {
            if (cbx != null)
            {
                cbx.BackColor = Color.White;
                cbx.Font = new Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular);
                //Txt.BorderStyle = BorderStyle.FixedSingle;
            }
            if (lbl != null)
            {
                lbl.Font = new Font("Segoe UI", lbl.Font.Size, System.Drawing.FontStyle.Regular);
            }
            //FrmPrincipal.frmPrinc.msgAyuda.Caption = "";
        }

        public static void AvanzaConEnter(ref KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)(Keys.Enter))
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

            if (e.KeyChar == (char)Keys.Escape)
            {
                e.Handled = true;
                SendKeys.Send("+{TAB}");
            }

        }

        public static bool ExisteDato(string tabla, string campo, string valor, SqlConnection con)
        {
            try
            {
                string sql = string.Format("SELECT * FROM {0} WHERE {1} = '{2}'", tabla, campo, valor);
                DataTable dt = Consultar(sql, con);
                if (dt.Rows.Count > 0) { return true; }else { return false; }
            }
            catch (Exception ex)
            {
                MensajeError(ex.Message);
                return false;
            }
        }

        public static DataTable Consultar(string sql, SqlConnection con)
        {
            try
            {
                SqlConnection conec = con;
                SqlCommand comando = new SqlCommand(sql, conec);
                SqlDataAdapter datos = new SqlDataAdapter(comando);
                DataTable tabla = new DataTable();
                try
                {
                    datos.Fill(tabla);
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                }
                return tabla;
            }
            catch (Exception)
            { return new DataTable(); }

        }

        public static DataTable ConsultarConSP(string SPname, SqlConnection con)
        {
            try
            {
                SqlConnection conec = con;
                SqlCommand comando = new SqlCommand(SPname, conec);
                comando.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter datos = new SqlDataAdapter(comando);
                DataTable tabla = new DataTable();
                try
                {
                    datos.Fill(tabla);
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                }
                return tabla;
            }
            catch (Exception)
            { return new DataTable(); }

        }

        public static List<T> DataReaderMapToList<T>(string sql)
        {
            List<T> list = new List<T>();
            try
            {
                using (SqlConnection conn = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                {
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        T obj = default(T);
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {
                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    prop.SetValue(obj, dr[prop.Name], null);
                                }
                            }
                            list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               
                return null;
            }
           
            return list;
        }

        public static string EjecutaComando(string sql, SqlConnection con)
        {
            sql = "set dateformat dmy; " + sql;
            SqlConnection conexion = con;
            SqlCommand comando = new SqlCommand(sql);
            comando.Connection = conexion;
            string val = "no";
            try
            {
                int fill_afec = comando.ExecuteNonQuery();
                if (fill_afec > 0)
                {
                    val = "si";
                }
            }
            catch (System.Exception ex)
            {
             
                return ex.Message.ToString();
            }
            finally
            {
            }
            return val;
        }

        public static int EjecutaComandoReturId(string sql, SqlConnection con)
        {
            sql = "set dateformat dmy; " + sql;
            SqlConnection conexion = con;
            SqlCommand comando = new SqlCommand(sql);
            comando.Connection = conexion;
            try
            {
                var fill_afec =  comando.ExecuteScalar();
                return  Convert.ToInt32(fill_afec);
            }
            catch (System.Exception a)
            {
                return -1;
            }
            finally
            {
            }
        }

        public static int TraeSiguienteSecuencial(string tabla, string campo, SqlConnection con)
        {
            try
            {
                string sql =string.Format("SELECT MAX(ISNULL({0},0))+1 AS Sec FROM {1}", campo,tabla);
                DataTable dt = Consultar(sql,con);
                if (dt.Rows.Count>0)
                {
                    return (int)(dt.Rows[0][0]);
                }else { return 0; }
            }
            catch (Exception ex)
            {
                MensajeError(ex.Message);
                return 0;
            }
        }

        public static byte[] GetHashKey(string hashKey)
        {
            // Initialize
            UTF8Encoding encoder = new UTF8Encoding();
            // Get the salt
            byte[] saltBytes = encoder.GetBytes(hashKey);
            // Setup the hasher
            Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(hashKey, saltBytes);
            // Return the key
            return rfc.GetBytes(16);
        }
        public static string Encrypt(string dataToEncrypt)
        {
            // Initialize
            AesManaged encryptor = new AesManaged();
            // Set the key
            encryptor.Key = Encoding.UTF8.GetBytes("ArlingtonVargasMahecha1117521997");
            encryptor.IV = Encoding.UTF8.GetBytes("ArlingtonVargasM");
            // create a memory stream
            using (MemoryStream encryptionStream = new MemoryStream())
            {
                // Create the crypto stream
                using (CryptoStream encrypt = new CryptoStream(encryptionStream, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    // Encrypt
                    byte[] utfD1 = UTF8Encoding.UTF8.GetBytes(dataToEncrypt);
                    encrypt.Write(utfD1, 0, utfD1.Length);
                    encrypt.FlushFinalBlock();
                    encrypt.Close();
                    // Return the encrypted data
                    return Convert.ToBase64String(encryptionStream.ToArray());
                }
            }
        }
        public static string Decrypt(string encryptedString)
        {
            // Initialize
            AesManaged decryptor = new AesManaged();
            byte[] encryptedData = Convert.FromBase64String(encryptedString);
            // Set the key
            decryptor.Key = Encoding.UTF8.GetBytes("ArlingtonVargasMahecha1117521997");
            decryptor.IV = Encoding.UTF8.GetBytes("ArlingtonVargasM");
            // create a memory stream
            using (MemoryStream decryptionStream = new MemoryStream())
            {
                // Create the crypto stream
                using (CryptoStream decrypt = new CryptoStream(decryptionStream, decryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    // Encrypt
                    decrypt.Write(encryptedData, 0, encryptedData.Length);
                    decrypt.Flush();
                    decrypt.Close();
                    // Return the unencrypted data
                    byte[] decryptedData = decryptionStream.ToArray();
                    return UTF8Encoding.UTF8.GetString(decryptedData, 0, decryptedData.Length);
                }
            }
        }
        public static string GuardarImagen(string SQL,SqlConnection con,  System.Drawing.Image Imagen)
        {
            SQL = "set dateformat dmy; " + SQL;
            SqlConnection conexion = con;
            SqlCommand comando = new SqlCommand(SQL);
            comando.Connection = conexion;
            string val = "no";
            try
            {
                comando.Parameters.Add("@imagen", SqlDbType.Image);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                Imagen.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                comando.Parameters["@imagen"].Value = ms.GetBuffer();
                int fillafect = comando.ExecuteNonQuery();
                if (fillafect > 0)
                    val = "si";
            }
            catch (System.Exception e)
            {
                val = e.Message;
            }
            finally
            {
            }
            return val;
        }

        //public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        imageIn.Save(ms, imageIn.RawFormat);
        //        return ms.ToArray();
        //    }
        //}

        //public Image byteArrayToImage(byte[] byteArrayIn)
        //{
        //    using (var ms = new MemoryStream(byteArrayIn))
        //    {
        //        return Image.FromStream(ms);
        //    }
        //    //MemoryStream ms = new MemoryStream(byteArrayIn);
        //    //Image returnImage = Image.FromStream(ms);
        //    //return returnImage;
        //}

        public static Image TraerImagenArchivo()
        {
            OpenFileDialog BImg = new OpenFileDialog();
            Image img;
            BImg.Filter = "Imagenes JPG|*.jpg";
            BImg.RestoreDirectory = true;
            if (BImg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                img = Image.FromFile(BImg.FileName);
            else
                img = null;

            return img;
        }
        public static Image byteArrayToImage(byte[] CampoImg)
        {
            try
            {
                Image img = DevExpress.XtraEditors.Controls.ByteImageConverter.FromByteArray(CampoImg);
                return img;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
        public static byte[] ImageToByteArray(Image ImagenConv)
        {
            try
            {
                byte[] img = DevExpress.XtraEditors.Controls.ByteImageConverter.ToByteArray(ImagenConv, ImagenConv.RawFormat);
                return img;
            }
            catch (Exception ex)
            {

                throw;
            }
           
        }

        //public static string CrearFiltro(DataTable tabla, string texto)
        //{
        //    try
        //    {
        //        texto = texto.Replace(' ', ';');
        //        string[] cadenas = texto.Split(';');
        //        string filtro = "";
        //        for (int i = 0; i < cadenas.Length; i++)
        //        {
        //            if (cadenas[i] != "" && cadenas[i] != " ")
        //            {
        //                if (i > 0)
        //                    filtro += " and ";
        //                filtro += " ( ";
        //                for (int o = 0; o < tabla.Columns.Count; o++)
        //                {
        //                    filtro += " CONVERT(" + tabla.Columns[o].ColumnName + ", System.String) like '%" + cadenas[i] + "%' ";
        //                    if (o < tabla.Columns.Count - 1)
        //                        filtro += " or ";
        //                }
        //                filtro += " ) ";
        //            }
        //        }
        //        return filtro;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
           
        //}
    }
}
//public class GrillaDevExpress
//{
//    public static GridView CrearGrilla(bool mostrarTitulo, bool activarFiltro, string tituloGrilla = "")
//    {
//        GridView Vista = new GridView();
//        Vista.OptionsView.ShowViewCaption = mostrarTitulo;
//        if (mostrarTitulo)
//            Vista.ViewCaption = tituloGrilla;

//        Vista.OptionsView.ColumnAutoWidth = true;
//        Vista.ActiveFilterEnabled = activarFiltro;
//        Vista.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
//        Vista.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
//        Vista.OptionsBehavior.Editable = false;
//        Vista.OptionsSelection.EnableAppearanceFocusedRow = true;
//        // Opciones de Manejo x Usuario Final                                                                                                                                            
//        Vista.OptionsCustomization.AllowRowSizing = false;
//        Vista.OptionsCustomization.AllowColumnResizing = false;
//        Vista.OptionsCustomization.AllowColumnMoving = false;
//        Vista.OptionsCustomization.AllowFilter = true;
//        Vista.OptionsCustomization.AllowSort = true;
//        //Opciones de Vista del Grid                                                                                                                                                    
//        Vista.OptionsView.ShowGroupPanel = false;
//        Vista.OptionsView.ShowAutoFilterRow = true;
//        Vista.OptionsView.ColumnAutoWidth = true;
//        Vista.OptionsView.ShowFooter = false;
//        //'Panel Inferior de Filtro                                                                                                                                                       
//        Vista.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
//        Vista.OptionsMenu.ShowAutoFilterRowItem = false;
//        Vista.OptionsMenu.ShowGroupSortSummaryItems = false;
//        Vista.OptionsMenu.EnableColumnMenu = true;
//        Vista.OptionsMenu.EnableGroupPanelMenu = false;
//        Vista.OptionsMenu.ShowGroupSortSummaryItems = false;
//        // ' Manejo de Colores   
//        Vista.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
//        Vista.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
//        Vista.Appearance.FocusedRow.Options.UseTextOptions = false;
//        Vista.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
//        return Vista;
//    }
//    /// <summary>
//    /// 
//    /// </summary>
//    /// <param name="nombre">Nombre de la columna del datasource</param>
//    /// <param name="titulo">Titulo del encabezado de columna</param>
//    /// <returns></returns>
//    public static GridColumn CrearColumna(string nombre, string titulo, FormatType tipo = FormatType.None, string formato = "", bool visible = true, bool autoAncho = false, int ancho = 100, bool soloLectura = true, bool permiteEditar = false, bool permiteFoco = false, HorzAlignment alineacionEncabezado = HorzAlignment.Center, HorzAlignment alineacion = HorzAlignment.Default)
//    {
//        GridColumn Columna = new GridColumn();
//        Columna.Name = Columna.FieldName = nombre;
//        Columna.Caption = titulo;
//        Columna.DisplayFormat.FormatType = tipo;
//        Columna.DisplayFormat.FormatString = formato;
//        Columna.Visible = visible;
//        Columna.OptionsColumn.ReadOnly = soloLectura;
//        Columna.OptionsColumn.AllowEdit = permiteEditar;
//        Columna.OptionsColumn.AllowFocus = permiteFoco;
//        Columna.AppearanceCell.Options.UseTextOptions = true;
//        Columna.AppearanceCell.TextOptions.HAlignment = alineacion;
//        Columna.AppearanceHeader.Options.UseTextOptions = true;
//        Columna.AppearanceHeader.TextOptions.HAlignment = alineacionEncabezado;
//        if (autoAncho)
//            Columna.BestFit();
//        else
//        {
//            Columna.OptionsColumn.FixedWidth = true;
//            Columna.Width = ancho;
//        }
//        return Columna;
//    }
//}

