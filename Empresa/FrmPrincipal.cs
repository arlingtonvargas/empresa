﻿using DevExpress.Skins;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Empresa.ET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmPrincipal : DevExpress.XtraEditors.XtraForm
    {
        FrmLogin _frm = new FrmLogin();
        
        public FrmPrincipal(FrmLogin frm)
        {
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            InitializeComponent();
            _frm = frm;
        }     

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
           
            SkinContainerCollection skins = SkinManager.Default.Skins;
            for (int i = 0; i < skins.Count; i++)
            {
                repositoryItemComboBox1.Items.Add(skins[i].SkinName);
            }
            lblVersion.Caption = "V"+ClFunciones.VersionApp;
            try
            {   //PONER TIPO DE LETRA A TODO
                foreach (DevExpress.XtraBars.BarItem ctrl in barManager1.Items)
                {
                    Type t = ctrl.GetType();
                    if (t.Equals(typeof(DevExpress.XtraBars.BarButtonItem)) || t.Equals(typeof(DevExpress.XtraBars.BarSubItem)) || t.Equals(typeof(DevExpress.XtraBars.SkinBarSubItem)))
                    {
                        ctrl.ItemAppearance.Disabled.Font = new System.Drawing.Font("Sagoe UI", 8.25F);
                        ctrl.ItemAppearance.Disabled.Options.UseFont = true;
                        ctrl.ItemAppearance.Hovered.Font = new System.Drawing.Font("Sagoe UI", 8.25F);
                        ctrl.ItemAppearance.Hovered.Options.UseFont = true;
                        ctrl.ItemAppearance.Normal.Font = new System.Drawing.Font("Sagoe UI", 8.25F);
                        ctrl.ItemAppearance.Normal.Options.UseFont = true;
                        ctrl.ItemAppearance.Pressed.Font = new System.Drawing.Font("Sagoe UI", 8.25F);
                        ctrl.ItemAppearance.Pressed.Options.UseFont = true;
                    }
                }
            }
            catch (Exception)
            { throw; }

        }
        private void btnSalir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
            //_frm.txtContra.ValorTextBox = "";
            //_frm.Show();
        }


        private void btnCategorias_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
               
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmCategorias);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmCategorias frm = new FrmCategorias();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void mdiPrincipal_PageAdded(object sender, DevExpress.XtraTabbedMdi.MdiTabPageEventArgs e)
        {
            try
            {
                this.IsMdiContainer = true;
                mdiPrincipal.MdiParent = this;                
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void mdiPrincipal_PageRemoved(object sender, DevExpress.XtraTabbedMdi.MdiTabPageEventArgs e)
        {
            if (this.MdiChildren.Length <= 0)
            {
                this.IsMdiContainer = false;
                mdiPrincipal.MdiParent = null;
            }
        }

        private void FrmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            _frm.txtContra.ValorTextBox = "";
            _frm.Show();
        }

        private void btnCrearProd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmProductos);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmProductos frm = new FrmProductos();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnRegStock_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmRegStock);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmRegStock frm = new FrmRegStock();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnNuewRegStock_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmRegStock);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmRegStock frm = new FrmRegStock();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnRepRegStock_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmReportRegStock);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmReportRegStock frm = new FrmReportRegStock();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnRegVenta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmRegVentas);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmRegVentas frm = new FrmRegVentas();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnRepVentas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmReportVentas);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmReportVentas frm = new FrmReportVentas();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnRegGastos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmRegGastos);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmRegGastos frm = new FrmRegGastos();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnRepGastos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmReportGastos);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmReportGastos frm = new FrmReportGastos();
                    frm.MdiParent = this;
                    frm.Show();
                }               
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnRptProdutos_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmReporte);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmReporte frm = new FrmReporte();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnCerrar_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (ActiveMdiChild != null)
                    ActiveMdiChild.Close();
            }
            else
            {
                DialogResult result = XtraMessageBox.Show(this, "Seguro que desea salir del sistema?", "Saliendo del sistema",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void btnSeparados_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmListaSeparados);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmListaSeparados frm = new FrmListaSeparados();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }

        private void btnNuevoSeparado_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmSeparado);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmSeparado frm = new FrmSeparado(true, new SeparadosDetalleVistaListET(), new AbonosSeparadoListET(), new SeparadosVistaET());                   
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error : " + ex.Message);
            }
        }
    }
}
