﻿using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmRegStock : Form
    {
        bool COlumnsFit = false;
        DataTable dtDetalle = new DataTable();
        public FrmRegStock()
        {
            InitializeComponent();
        }

        private void FrmRegStock_Load(object sender, EventArgs e)
        {
            CreaGrilla();
            CrearTabla();
            LlenarControlProds();
            dtpFechaEnvio.DateTime = DateTime.Now;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtProd.ValorTextBox!="" && txtCant.ValorTextBox != "")
                {
                    DataRow[] filas = dtDetalle.Select("IdProd = "+txtProd.ValorTextBox);
                    if (filas.Length>0)
                    {
                        ClFunciones.MensajeError("El producto ya se encuntra en la lista.");
                        return;
                    }
                    DataRow fila = dtDetalle.NewRow();
                    fila["IdProd"] = txtProd.ValorTextBox;
                    fila["Cantidad"] = txtCant.ValorTextBox;
                    fila["Nombre"] = txtProd.lblDescripcion.Text;
                    dtDetalle.Rows.Add(fila);
                    dtDetalle.AcceptChanges();
                    gcProd.DataSource = dtDetalle;
                    txtProd.ValorTextBox = "";
                    txtCant.ValorTextBox = "";
                    txtProd.Focus();
                    if (!COlumnsFit) gvProd.Columns.ToList().ForEach(x=> { x.BestFit(); });
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (dtpFechaEnvio.EditValue==null)
            {
                ClFunciones.MensajeError("Debe seleccionar la fecha de envio.");
                dtpFechaEnvio.Focus();
                return;
            }
            else if (dtDetalle.Rows.Count==0)
            {
                ClFunciones.MensajeError("No se han cargado productos.");
                return;
            }
            bool res = GuardarRegistro(dtDetalle, Convert.ToDateTime(dtpFechaEnvio.EditValue), DateTime.Now);
            if (res)
            {
                ClFunciones.MensajeExitoso("Registro creado de manera correcta.");
                btnLimpiar_Click(sender, e);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            dtpFechaEnvio.EditValue = DateTime.Now;
            dtDetalle.Rows.Clear();
            gcProd.DataSource = dtDetalle;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CrearTabla()
        {
            dtDetalle.Columns.Add("IdRegistroStockDetalle", typeof(int));
            dtDetalle.Columns.Add("IdRegistroStock", typeof(int));
            dtDetalle.Columns.Add("IdProd", typeof(int));
            dtDetalle.Columns.Add("Cantidad", typeof(int));
            dtDetalle.Columns.Add("CantActual", typeof(int));
            dtDetalle.Columns.Add("CantFinal", typeof(int));
            dtDetalle.Columns.Add("Nombre", typeof(string));
        }

        private void LlenarControlProds()
        {
            try
            {
                string sql = "SELECT Sec AS Codigo, Nombre AS Descripcion FROM Productos";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                txtProd.DataTable = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit = new RepositoryItemButtonEdit();
        private void CreaGrilla()
        {
            try
            {
                gvProd = GrillaDevExpress.CrearGrilla(false, false, "");
                gvProd.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvProd.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("IdRegistroStockDetalle", "", visible: false));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("IdRegistroStock", "", visible: false));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("IdProd", "Id prod", ancho: 50));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Nombre", "Producto", ancho: 150));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Cantidad", "Cantidad agregada", ancho: 70));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("CantActual", "Cant actual",  ancho: 70));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("CantFinal", "Cant final", ancho: 70));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("quitar", "Quitar", ancho: 100, permiteEditar: true, permiteFoco: true));
                gvProd.OptionsBehavior.Editable = true;
                gvProd.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvProd.OptionsCustomization.AllowColumnResizing = true;
                repositoryItemButtonEdit.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
                repositoryItemButtonEdit.Buttons[0].Width = 90;
                repositoryItemButtonEdit.Buttons[0].Caption = "Quitar producto";
                gvProd.Columns["quitar"].ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
                gvProd.Columns["quitar"].ColumnEdit = repositoryItemButtonEdit;
                gvProd.OptionsView.ColumnAutoWidth = false;
                gcProd.MainView = gvProd;
                this.gvProd.DoubleClick += new System.EventHandler(this.gvProd_DoubleClick);
                this.repositoryItemButtonEdit.Click += new System.EventHandler(this.repositoryItemButtonEdit1_Click);

            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }


        private void gvProd_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private bool GuardarRegistro(DataTable dtProds, DateTime fechaEnvio, DateTime fechaCreacion)
        {
            try
            {
                string sql = "";
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        int Sec = ClFunciones.TraeSiguienteSecuencial("RegistroStock", "Sec", con);
                        if (Sec>0)
                        {
                            sql = string.Format("INSERT INTO RegistroStock (Sec, FechaReg, FechaEnvio) VALUES({0}," +
                            " '{1:dd/MM/yyyy HH:mm:ss}', '{2:dd/MM/yyyy HH:mm:ss}')", Sec, fechaCreacion, fechaEnvio);
                            string res = ClFunciones.EjecutaComando(sql, con);
                            if (res == "si")
                            {                                
                                foreach (DataRow item in dtProds.Rows)
                                {
                                    sql = "SELECT ISNULL(Cantidad,0) AS Cantidad FROM Productos WHERE Sec = " + item["IdProd"].ToString();
                                    DataTable dtProd = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                                    int secDet = ClFunciones.TraeSiguienteSecuencial("RegistroStockDetalle", "IdRegistroStockDetalle", con);
                                    sql = string.Format("INSERT INTO RegistroStockDetalle (IdRegistroStockDetalle," +
                                        " IdRegistroStock, IdProd, Cantidad,CantActual, CantFinal) VALUES " +
                                        " ({0}, {1}, {2}, {3},{4},{5})", secDet, Sec, item["IdProd"], item["Cantidad"], 
                                        dtProd.Rows[0][0], Convert.ToInt32(dtProd.Rows[0][0]) + Convert.ToInt32(item["Cantidad"]));
                                    res = ClFunciones.EjecutaComando(sql, con);
                                    if (res!="si")
                                    {
                                        return false;
                                    }
                                    sql = string.Format("UPDATE Productos SET Cantidad = {0} WHERE Sec = {1}",
                                        Convert.ToInt32(dtProd.Rows[0][0]) + Convert.ToInt32(item["Cantidad"]), item["IdProd"]);
                                    res = ClFunciones.EjecutaComando(sql, con);
                                    if (res != "si")
                                    {
                                        return false;
                                    }
                                }
                            }
                            scope.Complete();
                        }
                        
                        
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
                return false;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenarControlProds();
        }

        private void repositoryItemButtonEdit1_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow[] filas = dtDetalle.Select("IdProd = " + gvProd.GetFocusedRowCellValue("IdProd").ToString());
                dtDetalle.Rows.Remove(filas[0]);
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError("Error: "+ ex.Message, "Mensaje error");
            }           
        }

        private void FrmRegStock_SizeChanged(object sender, EventArgs e)
        {
            if (gvProd.Columns.Count == 0) return;
            gvProd.Columns["IdProd"].Width = (12 * (groupControl1.Width - 60) / 100);
            gvProd.Columns["Nombre"].Width = (50* (groupControl1.Width - 60) / 100);
            gvProd.Columns["Cantidad"].Width = (12 * (groupControl1.Width - 60) / 100);
            gvProd.Columns["CantActual"].Width = (10 * (groupControl1.Width - 60) / 100);
            gvProd.Columns["CantFinal"].Width = (10 * (groupControl1.Width - 60) / 100);
        }
    }
}
