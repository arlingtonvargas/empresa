﻿namespace Empresa
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.mnuArchivo = new DevExpress.XtraBars.BarSubItem();
            this.btnSalir = new DevExpress.XtraBars.BarButtonItem();
            this.mnuProductos = new DevExpress.XtraBars.BarSubItem();
            this.btnCrearProd = new DevExpress.XtraBars.BarButtonItem();
            this.btnRegStock = new DevExpress.XtraBars.BarButtonItem();
            this.btnRepProd = new DevExpress.XtraBars.BarButtonItem();
            this.btnCategorias = new DevExpress.XtraBars.BarButtonItem();
            this.mrnRegStock = new DevExpress.XtraBars.BarSubItem();
            this.btnNuewRegStock = new DevExpress.XtraBars.BarButtonItem();
            this.btnRepRegStock = new DevExpress.XtraBars.BarButtonItem();
            this.mnuVentas = new DevExpress.XtraBars.BarSubItem();
            this.btnRegVenta = new DevExpress.XtraBars.BarButtonItem();
            this.btnRepVentas = new DevExpress.XtraBars.BarButtonItem();
            this.btnSeparados = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGastos = new DevExpress.XtraBars.BarSubItem();
            this.btnRegGastos = new DevExpress.XtraBars.BarButtonItem();
            this.btnRepGastos = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.btnRptProdutos = new DevExpress.XtraBars.BarButtonItem();
            this.skinBarSubItem1 = new DevExpress.XtraBars.SkinBarSubItem();
            this.btnCerrar = new DevExpress.XtraBars.BarButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.menuArchivo = new DevExpress.XtraBars.BarSubItem();
            this.mbtnSalir = new DevExpress.XtraBars.BarButtonItem();
            this.menuOpciones = new DevExpress.XtraBars.BarSubItem();
            this.menuProd = new DevExpress.XtraBars.BarSubItem();
            this.mbtnProds = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnCat = new DevExpress.XtraBars.BarButtonItem();
            this.menuRegStock = new DevExpress.XtraBars.BarSubItem();
            this.mbtnRegStok = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnRepRegStoc = new DevExpress.XtraBars.BarButtonItem();
            this.menuVentas = new DevExpress.XtraBars.BarSubItem();
            this.mbtnRegVenta = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnRepVentas = new DevExpress.XtraBars.BarButtonItem();
            this.menuGastos = new DevExpress.XtraBars.BarSubItem();
            this.mbtnRegGasto = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnRepGastos = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.lblMensaje = new DevExpress.XtraBars.BarStaticItem();
            this.lblVersion = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.mdiPrincipal = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.btnNuevoSeparado = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mdiPrincipal)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mnuProductos,
            this.mnuVentas,
            this.mnuArchivo,
            this.btnSalir,
            this.btnCrearProd,
            this.btnRegStock,
            this.btnRepProd,
            this.mnuGastos,
            this.btnRegVenta,
            this.btnRepVentas,
            this.btnRegGastos,
            this.btnRepGastos,
            this.btnCategorias,
            this.mrnRegStock,
            this.btnNuewRegStock,
            this.btnRepRegStock,
            this.skinBarSubItem1,
            this.mbtnSalir,
            this.menuArchivo,
            this.menuOpciones,
            this.menuProd,
            this.menuVentas,
            this.menuGastos,
            this.mbtnProds,
            this.mbtnCat,
            this.menuRegStock,
            this.mbtnRegStok,
            this.mbtnRepRegStoc,
            this.mbtnRegVenta,
            this.mbtnRepVentas,
            this.mbtnRegGasto,
            this.mbtnRepGastos,
            this.barSubItem1,
            this.btnRptProdutos,
            this.btnCerrar,
            this.lblMensaje,
            this.lblVersion,
            this.btnSeparados,
            this.barSubItem2,
            this.btnNuevoSeparado});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 46;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Herramientas";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(52, 168);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuArchivo),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuProductos, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuVentas, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGastos, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.skinBarSubItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCerrar, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.RotateWhenVertical = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Herramientas";
            // 
            // mnuArchivo
            // 
            this.mnuArchivo.Caption = "Archivo";
            this.mnuArchivo.Id = 4;
            this.mnuArchivo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuArchivo.ImageOptions.Image")));
            this.mnuArchivo.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuArchivo.ImageOptions.LargeImage")));
            this.mnuArchivo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSalir)});
            this.mnuArchivo.Name = "mnuArchivo";
            this.mnuArchivo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnSalir
            // 
            this.btnSalir.Caption = "Salir";
            this.btnSalir.Id = 5;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSalir_ItemClick);
            // 
            // mnuProductos
            // 
            this.mnuProductos.Caption = "Productos";
            this.mnuProductos.Id = 1;
            this.mnuProductos.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuProductos.ImageOptions.Image")));
            this.mnuProductos.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuProductos.ImageOptions.LargeImage")));
            this.mnuProductos.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCrearProd),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRegStock),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRepProd),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCategorias),
            new DevExpress.XtraBars.LinkPersistInfo(this.mrnRegStock)});
            this.mnuProductos.Name = "mnuProductos";
            this.mnuProductos.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnCrearProd
            // 
            this.btnCrearProd.Caption = "Productos";
            this.btnCrearProd.Id = 6;
            this.btnCrearProd.Name = "btnCrearProd";
            this.btnCrearProd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCrearProd_ItemClick);
            // 
            // btnRegStock
            // 
            this.btnRegStock.Caption = "Regsitrar en stock";
            this.btnRegStock.Id = 7;
            this.btnRegStock.Name = "btnRegStock";
            this.btnRegStock.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnRegStock.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRegStock_ItemClick);
            // 
            // btnRepProd
            // 
            this.btnRepProd.Caption = "Reporte productos";
            this.btnRepProd.Id = 8;
            this.btnRepProd.Name = "btnRepProd";
            this.btnRepProd.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnCategorias
            // 
            this.btnCategorias.Caption = "Categorias";
            this.btnCategorias.Id = 14;
            this.btnCategorias.Name = "btnCategorias";
            this.btnCategorias.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCategorias_ItemClick);
            // 
            // mrnRegStock
            // 
            this.mrnRegStock.Caption = "Registro en stock";
            this.mrnRegStock.Id = 15;
            this.mrnRegStock.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNuewRegStock),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRepRegStock)});
            this.mrnRegStock.Name = "mrnRegStock";
            // 
            // btnNuewRegStock
            // 
            this.btnNuewRegStock.Caption = "Nuevo";
            this.btnNuewRegStock.Id = 16;
            this.btnNuewRegStock.Name = "btnNuewRegStock";
            this.btnNuewRegStock.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNuewRegStock_ItemClick);
            // 
            // btnRepRegStock
            // 
            this.btnRepRegStock.Caption = "Reporte";
            this.btnRepRegStock.Id = 17;
            this.btnRepRegStock.Name = "btnRepRegStock";
            this.btnRepRegStock.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRepRegStock_ItemClick);
            // 
            // mnuVentas
            // 
            this.mnuVentas.Caption = "Ventas";
            this.mnuVentas.Id = 2;
            this.mnuVentas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuVentas.ImageOptions.Image")));
            this.mnuVentas.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuVentas.ImageOptions.LargeImage")));
            this.mnuVentas.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRegVenta),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRepVentas),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2)});
            this.mnuVentas.Name = "mnuVentas";
            this.mnuVentas.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnRegVenta
            // 
            this.btnRegVenta.Caption = "Registrar ventas";
            this.btnRegVenta.Id = 10;
            this.btnRegVenta.Name = "btnRegVenta";
            this.btnRegVenta.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRegVenta_ItemClick);
            // 
            // btnRepVentas
            // 
            this.btnRepVentas.Caption = "Reporte de ventas";
            this.btnRepVentas.Id = 11;
            this.btnRepVentas.Name = "btnRepVentas";
            this.btnRepVentas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRepVentas_ItemClick);
            // 
            // btnSeparados
            // 
            this.btnSeparados.Caption = "Lista Separados";
            this.btnSeparados.Id = 42;
            this.btnSeparados.Name = "btnSeparados";
            this.btnSeparados.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSeparados_ItemClick);
            // 
            // mnuGastos
            // 
            this.mnuGastos.Caption = "Gastos";
            this.mnuGastos.Id = 9;
            this.mnuGastos.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuGastos.ImageOptions.Image")));
            this.mnuGastos.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuGastos.ImageOptions.LargeImage")));
            this.mnuGastos.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRegGastos),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRepGastos)});
            this.mnuGastos.Name = "mnuGastos";
            this.mnuGastos.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnRegGastos
            // 
            this.btnRegGastos.Caption = "Registro de gastos";
            this.btnRegGastos.Id = 12;
            this.btnRegGastos.Name = "btnRegGastos";
            this.btnRegGastos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRegGastos_ItemClick);
            // 
            // btnRepGastos
            // 
            this.btnRepGastos.Caption = "Reporte de gastos";
            this.btnRepGastos.Id = 13;
            this.btnRepGastos.Name = "btnRepGastos";
            this.btnRepGastos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRepGastos_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Reportes";
            this.barSubItem1.Id = 37;
            this.barSubItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem1.ImageOptions.Image")));
            this.barSubItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem1.ImageOptions.LargeImage")));
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRptProdutos)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnRptProdutos
            // 
            this.btnRptProdutos.Caption = "Productos";
            this.btnRptProdutos.Id = 38;
            this.btnRptProdutos.Name = "btnRptProdutos";
            this.btnRptProdutos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRptProdutos_ItemClick);
            // 
            // skinBarSubItem1
            // 
            this.skinBarSubItem1.Caption = "Tema";
            this.skinBarSubItem1.Id = 18;
            this.skinBarSubItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("skinBarSubItem1.ImageOptions.Image")));
            this.skinBarSubItem1.Name = "skinBarSubItem1";
            this.skinBarSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Caption = "Cerrar";
            this.btnCerrar.Id = 39;
            this.btnCerrar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.ImageOptions.Image")));
            this.btnCerrar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCerrar.ImageOptions.LargeImage")));
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnCerrar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCerrar_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Menú principal";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.menuArchivo),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuOpciones, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Menú principal";
            // 
            // menuArchivo
            // 
            this.menuArchivo.Caption = "Archivo";
            this.menuArchivo.Id = 23;
            this.menuArchivo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnSalir)});
            this.menuArchivo.Name = "menuArchivo";
            // 
            // mbtnSalir
            // 
            this.mbtnSalir.Caption = "Salir";
            this.mbtnSalir.Id = 22;
            this.mbtnSalir.Name = "mbtnSalir";
            this.mbtnSalir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSalir_ItemClick);
            // 
            // menuOpciones
            // 
            this.menuOpciones.Caption = "Opciones";
            this.menuOpciones.Id = 24;
            this.menuOpciones.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.menuProd),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuVentas),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuGastos)});
            this.menuOpciones.Name = "menuOpciones";
            // 
            // menuProd
            // 
            this.menuProd.Caption = "Productos";
            this.menuProd.Id = 25;
            this.menuProd.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnProds),
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnCat),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuRegStock)});
            this.menuProd.Name = "menuProd";
            // 
            // mbtnProds
            // 
            this.mbtnProds.Caption = "Productos";
            this.mbtnProds.Id = 28;
            this.mbtnProds.Name = "mbtnProds";
            this.mbtnProds.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCrearProd_ItemClick);
            // 
            // mbtnCat
            // 
            this.mbtnCat.Caption = "Categorias";
            this.mbtnCat.Id = 29;
            this.mbtnCat.Name = "mbtnCat";
            this.mbtnCat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCategorias_ItemClick);
            // 
            // menuRegStock
            // 
            this.menuRegStock.Caption = "Registro en stock";
            this.menuRegStock.Id = 30;
            this.menuRegStock.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnRegStok),
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnRepRegStoc)});
            this.menuRegStock.Name = "menuRegStock";
            // 
            // mbtnRegStok
            // 
            this.mbtnRegStok.Caption = "Nuevo";
            this.mbtnRegStok.Id = 31;
            this.mbtnRegStok.Name = "mbtnRegStok";
            this.mbtnRegStok.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRegStock_ItemClick);
            // 
            // mbtnRepRegStoc
            // 
            this.mbtnRepRegStoc.Caption = "Reporte";
            this.mbtnRepRegStoc.Id = 32;
            this.mbtnRepRegStoc.Name = "mbtnRepRegStoc";
            this.mbtnRepRegStoc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRepRegStock_ItemClick);
            // 
            // menuVentas
            // 
            this.menuVentas.Caption = "Ventas";
            this.menuVentas.Id = 26;
            this.menuVentas.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnRegVenta),
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnRepVentas)});
            this.menuVentas.Name = "menuVentas";
            // 
            // mbtnRegVenta
            // 
            this.mbtnRegVenta.Caption = "Registrar ventas";
            this.mbtnRegVenta.Id = 33;
            this.mbtnRegVenta.Name = "mbtnRegVenta";
            this.mbtnRegVenta.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRegVenta_ItemClick);
            // 
            // mbtnRepVentas
            // 
            this.mbtnRepVentas.Caption = "Reporte de ventas";
            this.mbtnRepVentas.Id = 34;
            this.mbtnRepVentas.Name = "mbtnRepVentas";
            this.mbtnRepVentas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRepVentas_ItemClick);
            // 
            // menuGastos
            // 
            this.menuGastos.Caption = "Gastos";
            this.menuGastos.Id = 27;
            this.menuGastos.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnRegGasto),
            new DevExpress.XtraBars.LinkPersistInfo(this.mbtnRepGastos)});
            this.menuGastos.Name = "menuGastos";
            // 
            // mbtnRegGasto
            // 
            this.mbtnRegGasto.Caption = "Registro de gastos";
            this.mbtnRegGasto.Id = 35;
            this.mbtnRegGasto.Name = "mbtnRegGasto";
            this.mbtnRegGasto.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRegGastos_ItemClick);
            // 
            // mbtnRepGastos
            // 
            this.mbtnRepGastos.Caption = "Reporte de gastos";
            this.mbtnRepGastos.Id = 36;
            this.mbtnRepGastos.Name = "mbtnRepGastos";
            this.mbtnRepGastos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRepGastos_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Barra de estado";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.lblMensaje),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblVersion)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Barra de estado";
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.lblMensaje.Id = 40;
            this.lblMensaje.Name = "lblMensaje";
            // 
            // lblVersion
            // 
            this.lblVersion.Caption = "V1.0";
            this.lblVersion.Id = 41;
            this.lblVersion.Name = "lblVersion";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(800, 53);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(800, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 53);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 372);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(800, 53);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 372);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // mdiPrincipal
            // 
            this.mdiPrincipal.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mdiPrincipal.AppearancePage.HeaderActive.Options.UseFont = true;
            this.mdiPrincipal.MdiParent = null;
            this.mdiPrincipal.TabPageWidth = 100;
            this.mdiPrincipal.PageAdded += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.mdiPrincipal_PageAdded);
            this.mdiPrincipal.PageRemoved += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.mdiPrincipal_PageRemoved);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Separados";
            this.barSubItem2.Id = 44;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSeparados),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNuevoSeparado)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // btnNuevoSeparado
            // 
            this.btnNuevoSeparado.Caption = "Nuevo Separado";
            this.btnNuevoSeparado.Id = 45;
            this.btnNuevoSeparado.Name = "btnNuevoSeparado";
            this.btnNuevoSeparado.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNuevoSeparado_ItemClick);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Stretch;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrincipal";
            this.Text = "Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPrincipal_FormClosed);
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mdiPrincipal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem mnuArchivo;
        private DevExpress.XtraBars.BarButtonItem btnSalir;
        private DevExpress.XtraBars.BarSubItem mnuProductos;
        private DevExpress.XtraBars.BarButtonItem btnCrearProd;
        private DevExpress.XtraBars.BarButtonItem btnRegStock;
        private DevExpress.XtraBars.BarButtonItem btnRepProd;
        private DevExpress.XtraBars.BarSubItem mnuVentas;
        private DevExpress.XtraBars.BarButtonItem btnRegVenta;
        private DevExpress.XtraBars.BarButtonItem btnRepVentas;
        private DevExpress.XtraBars.BarSubItem mnuGastos;
        private DevExpress.XtraBars.BarButtonItem btnRegGastos;
        private DevExpress.XtraBars.BarButtonItem btnRepGastos;
        private DevExpress.XtraBars.BarButtonItem btnCategorias;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager mdiPrincipal;
        private DevExpress.XtraBars.BarSubItem mrnRegStock;
        private DevExpress.XtraBars.BarButtonItem btnNuewRegStock;
        private DevExpress.XtraBars.BarButtonItem btnRepRegStock;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarSubItem menuArchivo;
        private DevExpress.XtraBars.BarButtonItem mbtnSalir;
        private DevExpress.XtraBars.BarSubItem menuOpciones;
        private DevExpress.XtraBars.BarSubItem menuProd;
        private DevExpress.XtraBars.BarButtonItem mbtnProds;
        private DevExpress.XtraBars.BarButtonItem mbtnCat;
        private DevExpress.XtraBars.BarSubItem menuVentas;
        private DevExpress.XtraBars.BarSubItem menuGastos;
        private DevExpress.XtraBars.BarSubItem menuRegStock;
        private DevExpress.XtraBars.BarButtonItem mbtnRegStok;
        private DevExpress.XtraBars.BarButtonItem mbtnRepRegStoc;
        private DevExpress.XtraBars.BarButtonItem mbtnRegVenta;
        private DevExpress.XtraBars.BarButtonItem mbtnRepVentas;
        private DevExpress.XtraBars.BarButtonItem mbtnRegGasto;
        private DevExpress.XtraBars.BarButtonItem mbtnRepGastos;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem btnRptProdutos;
        private DevExpress.XtraBars.BarButtonItem btnCerrar;
        private DevExpress.XtraBars.BarStaticItem lblMensaje;
        private DevExpress.XtraBars.BarStaticItem lblVersion;
        private DevExpress.XtraBars.BarButtonItem btnSeparados;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem btnNuevoSeparado;
    }
}

