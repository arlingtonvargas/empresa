﻿using dll;
namespace Empresa
{
    partial class FrmProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProductos));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCosto = new dll.Controles.ucLabelTextBox();
            this.txtVenta = new dll.Controles.ucLabelTextBox();
            this.txtCan = new dll.Controles.ucLabelTextBox();
            this.txtDes = new dll.Controles.ucLabelTextBox();
            this.txtCat = new dll.Controles.ucBusqueda();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.txtNom = new dll.Controles.ucLabelTextBox();
            this.gcProd = new DevExpress.XtraGrid.GridControl();
            this.gvProd = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvProd)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.btnRefresh);
            this.groupControl1.Controls.Add(this.btnLimpiar);
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Controls.Add(this.txtCan);
            this.groupControl1.Controls.Add(this.txtDes);
            this.groupControl1.Controls.Add(this.txtCat);
            this.groupControl1.Controls.Add(this.btnGuardar);
            this.groupControl1.Controls.Add(this.btnSalir);
            this.groupControl1.Controls.Add(this.txtNom);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(901, 147);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Información de categoría";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(816, 57);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 24);
            this.btnRefresh.TabIndex = 8;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(816, 86);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 9;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtCosto, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtVenta, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 113);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(811, 32);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // txtCosto
            // 
            this.txtCosto.AnchoTitulo = 90;
            this.txtCosto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCosto.EditMask = "";
            this.txtCosto.Location = new System.Drawing.Point(3, 3);
            this.txtCosto.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCosto.MensajeDeAyuda = null;
            this.txtCosto.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCosto.Name = "txtCosto";
            this.txtCosto.PermiteSoloNumeros = true;
            this.txtCosto.Size = new System.Drawing.Size(399, 24);
            this.txtCosto.TabIndex = 5;
            this.txtCosto.TextoTitulo = "Precio costo :";
            this.txtCosto.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtCosto.ValorTextBox = "";
            // 
            // txtVenta
            // 
            this.txtVenta.AnchoTitulo = 90;
            this.txtVenta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVenta.EditMask = "";
            this.txtVenta.Location = new System.Drawing.Point(408, 3);
            this.txtVenta.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtVenta.MensajeDeAyuda = null;
            this.txtVenta.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtVenta.Name = "txtVenta";
            this.txtVenta.PermiteSoloNumeros = true;
            this.txtVenta.Size = new System.Drawing.Size(400, 24);
            this.txtVenta.TabIndex = 6;
            this.txtVenta.TextoTitulo = "Precio venta :";
            this.txtVenta.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtVenta.ValorTextBox = "";
            // 
            // txtCan
            // 
            this.txtCan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCan.AnchoTitulo = 70;
            this.txtCan.EditMask = "";
            this.txtCan.Enabled = false;
            this.txtCan.Location = new System.Drawing.Point(687, 57);
            this.txtCan.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCan.MensajeDeAyuda = null;
            this.txtCan.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCan.Name = "txtCan";
            this.txtCan.PermiteSoloNumeros = true;
            this.txtCan.Size = new System.Drawing.Size(123, 24);
            this.txtCan.TabIndex = 3;
            this.txtCan.TextoTitulo = "Existencia :";
            this.txtCan.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtCan.ValorTextBox = "";
            // 
            // txtDes
            // 
            this.txtDes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDes.AnchoTitulo = 90;
            this.txtDes.EditMask = "";
            this.txtDes.Location = new System.Drawing.Point(5, 87);
            this.txtDes.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDes.MensajeDeAyuda = null;
            this.txtDes.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDes.Name = "txtDes";
            this.txtDes.PermiteSoloNumeros = false;
            this.txtDes.Size = new System.Drawing.Size(805, 24);
            this.txtDes.TabIndex = 4;
            this.txtDes.TextoTitulo = "Descripción :";
            this.txtDes.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtDes.ValorTextBox = "";
            // 
            // txtCat
            // 
            this.txtCat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCat.AnchoTextBox = 50;
            this.txtCat.AnchoTitulo = 90;
            this.txtCat.Location = new System.Drawing.Point(5, 26);
            this.txtCat.Margin = new System.Windows.Forms.Padding(0);
            this.txtCat.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtCat.MensajeDeAyuda = null;
            this.txtCat.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtCat.Name = "txtCat";
            this.txtCat.PermiteSoloNumeros = false;
            this.txtCat.Size = new System.Drawing.Size(807, 28);
            this.txtCat.TabIndex = 1;
            this.txtCat.TextoTitulo = "Categoría :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtCat.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtCat.ValorTextBox = "";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(816, 28);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 24);
            this.btnGuardar.TabIndex = 7;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(816, 116);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // txtNom
            // 
            this.txtNom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNom.AnchoTitulo = 90;
            this.txtNom.EditMask = "";
            this.txtNom.Location = new System.Drawing.Point(5, 57);
            this.txtNom.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNom.MensajeDeAyuda = null;
            this.txtNom.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNom.Name = "txtNom";
            this.txtNom.PermiteSoloNumeros = false;
            this.txtNom.Size = new System.Drawing.Size(677, 24);
            this.txtNom.TabIndex = 2;
            this.txtNom.TextoTitulo = "Nombre :";
            this.txtNom.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtNom.ValorTextBox = "";
            // 
            // gcProd
            // 
            this.gcProd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcProd.Location = new System.Drawing.Point(12, 164);
            this.gcProd.MainView = this.gvProd;
            this.gcProd.Name = "gcProd";
            this.gcProd.Size = new System.Drawing.Size(901, 267);
            this.gcProd.TabIndex = 4;
            this.gcProd.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvProd});
            // 
            // gvProd
            // 
            this.gvProd.GridControl = this.gcProd;
            this.gvProd.Name = "gvProd";
            // 
            // FrmProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 443);
            this.Controls.Add(this.gcProd);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmProductos";
            this.Text = "Productos";
            this.Load += new System.EventHandler(this.FrmProductos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvProd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private dll.Controles.ucLabelTextBox txtNom;
        private DevExpress.XtraGrid.GridControl gcProd;
        private DevExpress.XtraGrid.Views.Grid.GridView gvProd;
        private dll.Controles.ucBusqueda txtCat;
        private dll.Controles.ucLabelTextBox txtVenta;
        private dll.Controles.ucLabelTextBox txtCosto;
        private dll.Controles.ucLabelTextBox txtDes;
        private dll.Controles.ucLabelTextBox txtCan;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
    }
}