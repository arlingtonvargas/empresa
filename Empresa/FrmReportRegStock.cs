﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmReportRegStock : Form
    {
        public FrmReportRegStock()
        {
            InitializeComponent();
        }

        private void FrmReportRegStock_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            dtpFechaIni.DateTime = DateTime.Now;
            dtpFechaFin.DateTime = DateTime.Now;

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtpFechaIni.EditValue != null && dtpFechaFin.EditValue != null)
                {
                    string sql = string.Format("set dateformat dmy;SELECT Sec, FechaReg, FechaEnvio FROM RegistroStock" +
                        " WHERE FechaReg BETWEEN '{0:dd/MM/yyyy 00:00:00}' AND '{1:dd/MM/yyyy 23:59:59}'", 
                        dtpFechaIni.EditValue, dtpFechaFin.EditValue);
                    DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    if (dt.Rows.Count==0)
                    {
                        ClFunciones.MensajeError("No se encontraron registros.");
                        return;
                    }
                    gcRegistro.DataSource = dt;
                }
                

            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CrearGrilla()
        {
            try
            {
                gvRegistro = GrillaDevExpress.CrearGrilla(false, false, "");
                gvRegistro.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvRegistro.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvRegistro.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);                
                gvRegistro.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Id", ancho: 30));
                gvRegistro.Columns.Add(GrillaDevExpress.CrearColumna("FechaReg", "Fecha registro", tipo: DevExpress.Utils.FormatType.DateTime, formato:"dd/MM/yyyy hh:mm tt"));
                gvRegistro.Columns.Add(GrillaDevExpress.CrearColumna("FechaEnvio", "Fecha envio", tipo: DevExpress.Utils.FormatType.DateTime, formato: "dd/MM/yyyy hh:mm tt"));
                gvRegistro.OptionsBehavior.Editable = true;
                gvRegistro.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvRegistro.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvRegistro.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvRegistro.OptionsCustomization.AllowColumnResizing = true;
                gcRegistro.MainView = gvRegistro;
                this.gvRegistro.DoubleClick += new System.EventHandler(this.gvRegistro_DoubleClick);



                gvDetalle = GrillaDevExpress.CrearGrilla(false, false, "");
                gvDetalle.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvDetalle.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvDetalle.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);                
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("IdProd", "Id prod", ancho: 50));
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("Nombre", "Producto", ancho: 150));
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("Cantidad", "Cantidad agregada", ancho: 70));
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("CantActual", "Cant actual", ancho: 70));
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("CantFinal", "Cant final", ancho: 70));
                gvDetalle.OptionsBehavior.Editable = true;
                gvDetalle.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvDetalle.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvDetalle.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvDetalle.OptionsCustomization.AllowColumnResizing = true;
                gcDetalle.MainView = gvDetalle;


            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void gvRegistro_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
                if (numFila > -1)
                {
                    int Sec = Convert.ToInt32(gvRegistro.GetDataRow(numFila)["Sec"]);
                    COnsultarDetalle(Sec);
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void COnsultarDetalle(int Sec)
        {
            try
            {
                string sql = "SELECT IdProd, PD.Nombre, RD.Cantidad, CantActual, CantFinal FROM RegistroStockDetalle RD " +
                " INNER JOIN Productos PD ON RD.IdProd = PD.Sec" +
                " WHERE IdRegistroStock = "+ Sec.ToString();
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                gcDetalle.DataSource = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }
    }
}
