﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmReportVentas : Form
    {
        public FrmReportVentas()
        {
            InitializeComponent();
        }

        private void FrmReportVentas_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            dtpFecha.DateTime = DateTime.Now;
            dtpFechaFin.DateTime = DateTime.Now;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtpFecha.EditValue != null && dtpFechaFin.EditValue != null)
                {
                    string sql = string.Format("set dateformat dmy;SELECT Sec, FechaVenta, FechaRegistro, SUM(VD.ValTo) AS ValTo FROM Ventas VT " +
                        " INNER JOIN VentaDetalle VD ON VT.Sec = VD.IdVenta" +
                        " WHERE FechaRegistro BETWEEN '{0:dd/MM/yyyy 00:00:00}' AND '{1:dd/MM/yyyy 23:59:59}'" +
                        " GROUP BY Sec, FechaVenta, FechaRegistro",
                        dtpFecha.EditValue, dtpFechaFin.EditValue);
                    DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    if (dt.Rows.Count == 0)
                    {
                        ClFunciones.MensajeError("No se encontraron registros.");
                        return;
                    }
                    gcVentas.DataSource = dt;
                }


            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void CrearGrilla()
        {
            try
            {
                gvVentas = GrillaDevExpress.CrearGrilla(false, false, "");
                gvVentas.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvVentas.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvVentas.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvVentas.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Id", ancho: 30));
                gvVentas.Columns.Add(GrillaDevExpress.CrearColumna("FechaRegistro", "Fecha registro", tipo: DevExpress.Utils.FormatType.DateTime, formato: "dd/MM/yyyy hh:mm tt"));
                gvVentas.Columns.Add(GrillaDevExpress.CrearColumna("FechaVenta", "Fecha venta", tipo: DevExpress.Utils.FormatType.DateTime, formato: "dd/MM/yyyy hh:mm tt"));
                gvVentas.Columns.Add(GrillaDevExpress.CrearColumna("ValTo", "Valor Venta", ancho: 70, tipo:DevExpress.Utils.FormatType.Numeric, formato:"C2"));
                gvVentas.OptionsBehavior.Editable = true;
                gvVentas.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvVentas.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvVentas.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvVentas.OptionsCustomization.AllowColumnResizing = true;
                gvVentas.OptionsView.ShowFooter = true;
                gvVentas.Columns["FechaVenta"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
                gvVentas.Columns["FechaVenta"].SummaryItem.FieldName = "FechaVenta";
                gvVentas.Columns["FechaVenta"].SummaryItem.DisplayFormat = "Registros : {0}";
                gvVentas.Columns["ValTo"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                gvVentas.Columns["ValTo"].SummaryItem.FieldName = "ValTo";
                gvVentas.Columns["ValTo"].SummaryItem.DisplayFormat = "Total : {0:C2}";
                gcVentas.MainView = gvVentas;
                this.gvVentas.DoubleClick += new System.EventHandler(this.gvRegistro_DoubleClick);



                gvDetalle = GrillaDevExpress.CrearGrilla(false, false, "");
                gvDetalle.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvDetalle.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvDetalle.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("IdProd", "Id prod", ancho: 30));
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("Nombre", "Producto", ancho: 150));
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("ValUni", "Cant actual", ancho: 70, tipo: DevExpress.Utils.FormatType.Numeric, formato: "C2"));
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("Cantidad", "Cantidad agregada", ancho: 70));
                gvDetalle.Columns.Add(GrillaDevExpress.CrearColumna("ValTo", "Cant final", ancho: 70, tipo:DevExpress.Utils.FormatType.Numeric, formato:"C2"));
                gvDetalle.OptionsBehavior.Editable = true;
                gvDetalle.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvDetalle.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvDetalle.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvDetalle.OptionsCustomization.AllowColumnResizing = true;
                gvDetalle.OptionsView.ShowFooter = true;
                gvDetalle.Columns["ValTo"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                gvDetalle.Columns["ValTo"].SummaryItem.FieldName = "ValTo";
                gvDetalle.Columns["ValTo"].SummaryItem.DisplayFormat = "Total : {0:C2}";
                gcDetalle.MainView = gvDetalle;


            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void gvRegistro_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
                if (numFila > -1)
                {
                    int Sec = Convert.ToInt32(gvVentas.GetDataRow(numFila)["Sec"]);
                    COnsultarDetalle(Sec);
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void COnsultarDetalle(int Sec)
        {
            try
            {
                string sql = "SELECT VD.IdProd, PD.Nombre, VD.Cantidad, VD.ValUni, VD.ValTo FROM VentaDetalle VD " +
                " INNER JOIN Productos PD ON VD.IdProd = PD.Sec " +
                " WHERE IdVenta = " + Sec.ToString();
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                gcDetalle.DataSource = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }
    }
}
