﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa
{
    public class ClConexion
    {
        public SqlConnection Conexion;
        public static string _Servidor = "";
        bool vEstacConectado = false;
        private string cadCon = System.Configuration.ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString; 

        public static ClConexion clConexion = new ClConexion();

        public string Servidor
        {
            get
            {
                return _Servidor;
            }
            set
            {
                _Servidor = value;
                cadCon = System.Configuration.ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString;                
                //cadCon = string.Format("Data Source={0};Initial Catalog=BDIEJAG;user id = sa; password = 2121121512", Servidor);
                //cadCon = string.Format("Data Source={0};Initial Catalog=DB_A4124B_IEJAG;User Id=DB_A4124B_IEJAG_admin;Password=3114556084eA.;", value);
            }
        } 
        public string CadenaConexion
        {
            get
            {
                return cadCon;
            }
        } 


        public bool Ingreso()
        {
            try
            {
                //Servidor = server;
                Conexion = new SqlConnection(CadenaConexion);
                Conexion.Open();
                return true;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
                return false;
            }

        }
        public void DesconectaBD()
        {
            Conexion.Close();
            EstaConectado = false;
        }

        public bool EstaConectado
        {
            get
            {
                return vEstacConectado;
            }
            set
            {
                vEstacConectado = value;
            }
        }
    }
}
