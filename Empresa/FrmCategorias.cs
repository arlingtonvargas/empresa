﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmCategorias : Form
    {
        int IdCat = -1;
        bool estaActualizando = false;
        public FrmCategorias()
        {
            InitializeComponent();
        }

        private void FrmCategorias_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenarGrilla();
            txtCat.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtCat.ValorTextBox !="")
            {
                if (GuardarCat(txtCat.ValorTextBox))
                {
                    if (estaActualizando)
                    {
                        ClFunciones.MensajeExitoso("Categoria actualizada correctamente.");
                    }
                    else
                    {
                        ClFunciones.MensajeExitoso("Categoria creada correctamente.");
                    }
                    LlenarGrilla();
                    txtCat.ValorTextBox = "";
                    IdCat = -1;
                    estaActualizando = false;
                }
                else
                {
                    ClFunciones.MensajeError("Lo sentimos, ha ocurrido un error.");
                }
                txtCat.Focus();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CrearGrilla()
        {
            try
            {
                gvCat = GrillaDevExpress.CrearGrilla(false, false, "");
                gvCat.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvCat.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvCat.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvCat.Columns.Add(GrillaDevExpress.CrearColumna("IdCat", "Id", ancho: 20));
                gvCat.Columns.Add(GrillaDevExpress.CrearColumna("NomCat", "Nombre", ancho: 220));
                gvCat.OptionsBehavior.Editable = true;
                gvCat.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvCat.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvCat.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvCat.OptionsCustomization.AllowColumnResizing = true;
                gcCat.MainView = gvCat;
                this.gvCat.DoubleClick += new System.EventHandler(this.gvCat_DoubleClick);
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void gvCat_DoubleClick(object sender, EventArgs e)
        {
            int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
            if (numFila>-1)
            {
                IdCat = Convert.ToInt32(gvCat.GetDataRow(numFila)["IdCat"]);
                txtCat.ValorTextBox = gvCat.GetDataRow(numFila)["NomCat"].ToString();
                estaActualizando = true;
                txtCat.Focus();
            }
        }

        private void LlenarGrilla()
        {
            try
            {
                string sql = "SELECT IdCat, NomCat FROM Categorias";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                gcCat.DataSource = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        public bool GuardarCat(string nomCat)
        {
            try
            {
                if (estaActualizando)
                {
                    string sql = string.Format("UPDATE Categorias SET NomCat = '{0}' WHERE IdCat = {1}",nomCat, IdCat);
                    string res = ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                    if (res == "si")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    int Sec = ClFunciones.TraeSiguienteSecuencial("Categorias", "IdCat", ClConexion.clConexion.Conexion);
                    if (Sec > 0)
                    {
                        string sql = string.Format("INSERT INTO Categorias (IdCat, NomCat) VALUES ({0},'{1}')", Sec, nomCat);
                        string res = ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                        if (res == "si")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }               
                
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
                return false;
            }
        }
    }
}
