﻿namespace Empresa
{
    partial class FrmListaSeparados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmListaSeparados));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dtpFechaFin = new DevExpress.XtraEditors.DateEdit();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.dtpFechaIni = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtCant = new dll.Controles.ucLabelTextBox();
            this.gcSeparados = new DevExpress.XtraGrid.GridControl();
            this.separadosListaVistaETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvSeparados = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colValSeparado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValAbonado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaSeparo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaFinSeparo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCelCliente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaFin.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaFin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaIni.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaIni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSeparados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separadosListaVistaETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeparados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Controls.Add(this.txtCant);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(818, 66);
            this.groupControl1.TabIndex = 9;
            this.groupControl1.Text = "Busqueda";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel1.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dtpFechaFin, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSalir, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnLimpiar, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.dtpFechaIni, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl2, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(808, 30);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(74, 24);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Fecha desde :";
            // 
            // dtpFechaFin
            // 
            this.dtpFechaFin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpFechaFin.EditValue = new System.DateTime(((long)(0)));
            this.dtpFechaFin.Location = new System.Drawing.Point(406, 3);
            this.dtpFechaFin.Name = "dtpFechaFin";
            this.dtpFechaFin.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.dtpFechaFin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.dtpFechaFin.Properties.Appearance.Options.UseBackColor = true;
            this.dtpFechaFin.Properties.Appearance.Options.UseFont = true;
            this.dtpFechaFin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFechaFin.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFechaFin.Size = new System.Drawing.Size(237, 24);
            this.dtpFechaFin.TabIndex = 2;
            // 
            // btnSalir
            // 
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(729, 3);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(76, 24);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(649, 3);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(74, 24);
            this.btnLimpiar.TabIndex = 3;
            this.btnLimpiar.Text = "Buscar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // dtpFechaIni
            // 
            this.dtpFechaIni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpFechaIni.EditValue = new System.DateTime(((long)(0)));
            this.dtpFechaIni.Location = new System.Drawing.Point(83, 3);
            this.dtpFechaIni.Name = "dtpFechaIni";
            this.dtpFechaIni.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.dtpFechaIni.Properties.Appearance.Options.UseFont = true;
            this.dtpFechaIni.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFechaIni.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFechaIni.Size = new System.Drawing.Size(237, 24);
            this.dtpFechaIni.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(326, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(74, 24);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "Fecha hasta :";
            // 
            // txtCant
            // 
            this.txtCant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCant.AnchoTitulo = 50;
            this.txtCant.EditMask = "";
            this.txtCant.Location = new System.Drawing.Point(1991, 57);
            this.txtCant.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCant.MensajeDeAyuda = null;
            this.txtCant.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCant.Name = "txtCant";
            this.txtCant.PermiteSoloNumeros = true;
            this.txtCant.Size = new System.Drawing.Size(144, 24);
            this.txtCant.TabIndex = 11;
            this.txtCant.TextoTitulo = "Cant :";
            this.txtCant.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtCant.ValorTextBox = "";
            // 
            // gcSeparados
            // 
            this.gcSeparados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSeparados.DataSource = this.separadosListaVistaETBindingSource;
            this.gcSeparados.Location = new System.Drawing.Point(12, 84);
            this.gcSeparados.MainView = this.gvSeparados;
            this.gcSeparados.Name = "gcSeparados";
            this.gcSeparados.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gcSeparados.Size = new System.Drawing.Size(818, 364);
            this.gcSeparados.TabIndex = 10;
            this.gcSeparados.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSeparados});
            // 
            // separadosListaVistaETBindingSource
            // 
            this.separadosListaVistaETBindingSource.DataSource = typeof(Empresa.ET.SeparadosListaVistaET);
            // 
            // gvSeparados
            // 
            this.gvSeparados.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colValSeparado,
            this.colValAbonado,
            this.colSec,
            this.colFechaSeparo,
            this.colFechaFinSeparo,
            this.colFechaRegistro,
            this.colCelCliente,
            this.gridColumn1});
            this.gvSeparados.GridControl = this.gcSeparados;
            this.gvSeparados.Name = "gvSeparados";
            // 
            // colValSeparado
            // 
            this.colValSeparado.DisplayFormat.FormatString = "C2";
            this.colValSeparado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValSeparado.FieldName = "ValSeparado";
            this.colValSeparado.Name = "colValSeparado";
            this.colValSeparado.OptionsColumn.AllowEdit = false;
            this.colValSeparado.OptionsColumn.AllowFocus = false;
            this.colValSeparado.Visible = true;
            this.colValSeparado.VisibleIndex = 5;
            this.colValSeparado.Width = 109;
            // 
            // colValAbonado
            // 
            this.colValAbonado.DisplayFormat.FormatString = "C2";
            this.colValAbonado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValAbonado.FieldName = "ValAbonado";
            this.colValAbonado.Name = "colValAbonado";
            this.colValAbonado.OptionsColumn.AllowEdit = false;
            this.colValAbonado.OptionsColumn.AllowFocus = false;
            this.colValAbonado.Visible = true;
            this.colValAbonado.VisibleIndex = 6;
            this.colValAbonado.Width = 116;
            // 
            // colSec
            // 
            this.colSec.FieldName = "Sec";
            this.colSec.Name = "colSec";
            this.colSec.OptionsColumn.AllowEdit = false;
            this.colSec.OptionsColumn.AllowFocus = false;
            this.colSec.Visible = true;
            this.colSec.VisibleIndex = 0;
            this.colSec.Width = 31;
            // 
            // colFechaSeparo
            // 
            this.colFechaSeparo.FieldName = "FechaSeparo";
            this.colFechaSeparo.Name = "colFechaSeparo";
            this.colFechaSeparo.OptionsColumn.AllowEdit = false;
            this.colFechaSeparo.OptionsColumn.AllowFocus = false;
            this.colFechaSeparo.Visible = true;
            this.colFechaSeparo.VisibleIndex = 1;
            this.colFechaSeparo.Width = 109;
            // 
            // colFechaFinSeparo
            // 
            this.colFechaFinSeparo.FieldName = "FechaFinSeparo";
            this.colFechaFinSeparo.Name = "colFechaFinSeparo";
            this.colFechaFinSeparo.OptionsColumn.AllowEdit = false;
            this.colFechaFinSeparo.OptionsColumn.AllowFocus = false;
            this.colFechaFinSeparo.Visible = true;
            this.colFechaFinSeparo.VisibleIndex = 2;
            this.colFechaFinSeparo.Width = 109;
            // 
            // colFechaRegistro
            // 
            this.colFechaRegistro.FieldName = "FechaRegistro";
            this.colFechaRegistro.Name = "colFechaRegistro";
            this.colFechaRegistro.OptionsColumn.AllowEdit = false;
            this.colFechaRegistro.OptionsColumn.AllowFocus = false;
            this.colFechaRegistro.Visible = true;
            this.colFechaRegistro.VisibleIndex = 3;
            this.colFechaRegistro.Width = 109;
            // 
            // colCelCliente
            // 
            this.colCelCliente.FieldName = "CelCliente";
            this.colCelCliente.Name = "colCelCliente";
            this.colCelCliente.OptionsColumn.AllowEdit = false;
            this.colCelCliente.OptionsColumn.AllowFocus = false;
            this.colCelCliente.Visible = true;
            this.colCelCliente.VisibleIndex = 4;
            this.colCelCliente.Width = 109;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Ver", 65, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Ver";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.MaxWidth = 75;
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            // 
            // FrmListaSeparados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 460);
            this.Controls.Add(this.gcSeparados);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmListaSeparados";
            this.Text = "Lista Separados";
            this.Load += new System.EventHandler(this.FrmListaSeparados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaFin.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaFin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaIni.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaIni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSeparados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separadosListaVistaETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeparados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dtpFechaFin;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.DateEdit dtpFechaIni;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private dll.Controles.ucLabelTextBox txtCant;
        private DevExpress.XtraGrid.GridControl gcSeparados;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSeparados;
        private System.Windows.Forms.BindingSource separadosListaVistaETBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colValSeparado;
        private DevExpress.XtraGrid.Columns.GridColumn colValAbonado;
        private DevExpress.XtraGrid.Columns.GridColumn colSec;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaSeparo;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaFinSeparo;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaRegistro;
        private DevExpress.XtraGrid.Columns.GridColumn colCelCliente;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
    }
}