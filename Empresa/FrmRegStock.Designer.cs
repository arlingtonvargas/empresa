﻿namespace Empresa
{
    partial class FrmRegStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegStock));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.txtCant = new dll.Controles.ucLabelTextBox();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dtpFechaEnvio = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ucLabelTextBox1 = new dll.Controles.ucLabelTextBox();
            this.txtProd = new dll.Controles.ucBusqueda();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.gcProd = new DevExpress.XtraGrid.GridControl();
            this.gvProd = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEnvio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEnvio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvProd)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.btnRefresh);
            this.groupControl1.Controls.Add(this.txtCant);
            this.groupControl1.Controls.Add(this.btnAdd);
            this.groupControl1.Controls.Add(this.btnLimpiar);
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Controls.Add(this.txtProd);
            this.groupControl1.Controls.Add(this.btnGuardar);
            this.groupControl1.Controls.Add(this.btnSalir);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1016, 86);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Información de producto";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(850, 55);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 10;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtCant
            // 
            this.txtCant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCant.AnchoTitulo = 50;
            this.txtCant.Location = new System.Drawing.Point(686, 55);
            this.txtCant.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCant.MensajeDeAyuda = null;
            this.txtCant.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCant.Name = "txtCant";
            this.txtCant.PermiteSoloNumeros = true;
            this.txtCant.Size = new System.Drawing.Size(110, 24);
            this.txtCant.TabIndex = 2;
            this.txtCant.TextoTitulo = "Cant :";
            this.txtCant.ValorTextBox = "";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(802, 56);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(38, 22);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(931, 25);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 9;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dtpFechaEnvio, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ucLabelTextBox1, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 23);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(839, 29);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // dtpFechaEnvio
            // 
            this.dtpFechaEnvio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpFechaEnvio.EditValue = new System.DateTime(((long)(0)));
            this.dtpFechaEnvio.Location = new System.Drawing.Point(83, 3);
            this.dtpFechaEnvio.Name = "dtpFechaEnvio";
            this.dtpFechaEnvio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.dtpFechaEnvio.Properties.Appearance.Options.UseFont = true;
            this.dtpFechaEnvio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFechaEnvio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFechaEnvio.Size = new System.Drawing.Size(144, 24);
            this.dtpFechaEnvio.TabIndex = 10;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(74, 23);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Fecha envio :";
            // 
            // ucLabelTextBox1
            // 
            this.ucLabelTextBox1.AnchoTitulo = 100;
            this.ucLabelTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucLabelTextBox1.Location = new System.Drawing.Point(233, 3);
            this.ucLabelTextBox1.MaximumSize = new System.Drawing.Size(3000, 24);
            this.ucLabelTextBox1.MensajeDeAyuda = null;
            this.ucLabelTextBox1.MinimumSize = new System.Drawing.Size(110, 24);
            this.ucLabelTextBox1.Name = "ucLabelTextBox1";
            this.ucLabelTextBox1.PermiteSoloNumeros = false;
            this.ucLabelTextBox1.Size = new System.Drawing.Size(603, 24);
            this.ucLabelTextBox1.TabIndex = 12;
            this.ucLabelTextBox1.TextoTitulo = "Fecha creación :";
            this.ucLabelTextBox1.ValorTextBox = "";
            // 
            // txtProd
            // 
            this.txtProd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProd.AnchoTextBox = 150;
            this.txtProd.AnchoTitulo = 77;
            this.txtProd.Location = new System.Drawing.Point(8, 55);
            this.txtProd.Margin = new System.Windows.Forms.Padding(0);
            this.txtProd.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtProd.MensajeDeAyuda = null;
            this.txtProd.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtProd.Name = "txtProd";
            this.txtProd.PermiteSoloNumeros = false;
            this.txtProd.Size = new System.Drawing.Size(665, 28);
            this.txtProd.TabIndex = 1;
            this.txtProd.TextoTitulo = "Producto :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtProd.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtProd.ValorTextBox = "";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(850, 26);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 7;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(931, 55);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 8;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gcProd
            // 
            this.gcProd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcProd.Location = new System.Drawing.Point(12, 104);
            this.gcProd.MainView = this.gvProd;
            this.gcProd.Name = "gcProd";
            this.gcProd.Size = new System.Drawing.Size(1016, 334);
            this.gcProd.TabIndex = 5;
            this.gcProd.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvProd});
            // 
            // gvProd
            // 
            this.gvProd.GridControl = this.gcProd;
            this.gvProd.Name = "gvProd";
            // 
            // FrmRegStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 450);
            this.Controls.Add(this.gcProd);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmRegStock";
            this.Text = "Registro en stock";
            this.Load += new System.EventHandler(this.FrmRegStock_Load);
            this.SizeChanged += new System.EventHandler(this.FrmRegStock_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEnvio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEnvio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvProd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private dll.Controles.ucBusqueda txtProd;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraGrid.GridControl gcProd;
        private DevExpress.XtraGrid.Views.Grid.GridView gvProd;
        private DevExpress.XtraEditors.DateEdit dtpFechaEnvio;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private dll.Controles.ucLabelTextBox ucLabelTextBox1;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private dll.Controles.ucLabelTextBox txtCant;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;

    }
}