﻿using DevExpress.XtraEditors;
using Empresa.ET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmSeparado : XtraForm
    {
        SeparadosDetalleVistaListET ListaSeparados = new SeparadosDetalleVistaListET();
        AbonosSeparadoListET ListAbonos = new AbonosSeparadoListET();
        bool isNewElement = false;
        SeparadosVistaET Separado = new SeparadosVistaET();
        public FrmSeparado(bool _isNewElement, SeparadosDetalleVistaListET _ListaSeparados, AbonosSeparadoListET _ListAbonos, SeparadosVistaET _separado)
        {
            InitializeComponent();
            isNewElement = _isNewElement;
            ListAbonos = _ListAbonos;
            ListaSeparados = _ListaSeparados;
            Separado = _separado;
            if (!isNewElement)
            {
                txtProd.Enabled = false;
                btnAdd.Enabled = false;
                abonosSeparadoListETBindingSource.DataSource = ListAbonos;
                separadosDetalleVistaListETBindingSource.DataSource = ListaSeparados;
                dtpFechaSep.dtpDate.DateTime = Separado.FechaSeparo;
                dtpFechaFinSep.dtpDate.DateTime = Separado.FechaFinSeparo;
                txtCelCli.ValorTextBox = Separado.CelCliente.ToString();
            }
        }


        private void FrmSeparados_Load(object sender, EventArgs e)
        {
            LlenarControlProds();
            separadosDetalleVistaListETBindingSource.DataSource = ListaSeparados;
            abonosSeparadoListETBindingSource.DataSource = ListAbonos;
        }

        private void FrmSeparados_SizeChanged(object sender, EventArgs e)
        {
            splitContainerControl1.SplitterPosition = (groupControl1.Width * 70) / 100;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtProd.ValorTextBox == "" || txtValUnit.ValorTextBox == "" || txtCantidad.ValorTextBox == "" || txtValto.ValorTextBox == "") return;
            if (ListaSeparados.Where(x => x.IdProd == Convert.ToInt32(txtProd.ValorTextBox)).ToList().Count > 0) return;
            SeparadosDetalleVistaET obj = new SeparadosDetalleVistaET()
            {
                Sec = 0,
                Cant = Convert.ToInt32(txtCantidad.ValorTextBox),
                FechaRegistro = DateTime.Now,
                IdProd = Convert.ToInt32(txtProd.ValorTextBox),
                NomProd = txtProd.lblDescripcion.Text,
                ValProd = Convert.ToDecimal(txtValUnit.ValorTextBox),
                ValTo = Convert.ToDecimal(txtValto.ValorTextBox)
            };
            ListaSeparados.Add(obj);
            gcDetallesSeparado.RefreshDataSource();
            btnLimpiar_Click(sender, e);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtpFechaSep.dtpDate.DateTime == null || dtpFechaFinSep.dtpDate.DateTime == null || ListaSeparados.Count == 0 || ListAbonos.Count == 0) return;
                bool inserto = false;
                using (TransactionScope scope = new TransactionScope())
                {

                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        if (!isNewElement)
                        {
                            ClFunciones.EjecutaComando("DELETE AbonosSeparado WHERE IdSeparado = " + Separado.Sec, con);
                            foreach (AbonosSeparadoET item in ListAbonos)
                            {
                                SqlCommand cmd = new SqlCommand("SP_AbonosSeparado_I", con);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@pSec", SqlDbType.Int).Direction = ParameterDirection.Output;
                                cmd.Parameters.AddWithValue("@pIdSeparado", this.Separado.Sec);
                                cmd.Parameters.AddWithValue("@pFechaAbono", item.FechaAbono);
                                cmd.Parameters.AddWithValue("@pValAbono", item.ValAbono);
                                cmd.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                                cmd.ExecuteNonQuery();
                                inserto = Convert.ToBoolean(cmd.Parameters["@Inserto"].Value);
                                if (!inserto) goto goError;
                            }
                            goto goComplete;
                        }
                        SqlCommand comando = new SqlCommand("SP_Separados_I", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.Add("@pSec", SqlDbType.Int).Direction = ParameterDirection.Output;
                        comando.Parameters.AddWithValue("@pFechaSeparo", dtpFechaSep.dtpDate.DateTime);
                        comando.Parameters.AddWithValue("@pFechaFinSeparo", dtpFechaFinSep.dtpDate.DateTime);
                        comando.Parameters.AddWithValue("@pCelCliente", txtCelCli.ValorTextBox == "" ? "0" : txtCelCli.ValorTextBox);
                        comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                        if (!inserto) goto goError;
                        int SecSeparado = Convert.ToInt32(comando.Parameters["@pSec"].Value);

                        foreach (SeparadosDetalleET item in ListaSeparados)
                        {
                            comando = new SqlCommand("SP_SeparadosDetalle_I", con);
                            comando.CommandType = CommandType.StoredProcedure;
                            comando.Parameters.Add("@pSec", SqlDbType.Int).Direction = ParameterDirection.Output;
                            comando.Parameters.AddWithValue("@pSecSeparado", SecSeparado);
                            comando.Parameters.AddWithValue("@pIdProd", item.IdProd);
                            comando.Parameters.AddWithValue("@pValProd", item.ValProd);
                            comando.Parameters.AddWithValue("@pCant", item.Cant);
                            comando.Parameters.AddWithValue("@pValTo", item.ValTo);
                            comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                            comando.ExecuteNonQuery();
                            inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                            if (!inserto) goto goError;
                        }
                        foreach (AbonosSeparadoET item in ListAbonos)
                        {
                            comando = new SqlCommand("SP_AbonosSeparado_I", con);
                            comando.CommandType = CommandType.StoredProcedure;
                            comando.Parameters.Add("@pSec", SqlDbType.Int).Direction = ParameterDirection.Output;
                            comando.Parameters.AddWithValue("@pIdSeparado", SecSeparado);
                            comando.Parameters.AddWithValue("@pFechaAbono", item.FechaAbono);
                            comando.Parameters.AddWithValue("@pValAbono", item.ValAbono);
                            comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                            comando.ExecuteNonQuery();
                            inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                            if (!inserto) goto goError;
                        }
                    }
                    goComplete:
                    scope.Complete();
                    ClFunciones.MensajeExitoso("Separado creado correctamente.");
                    ListaSeparados.Clear();
                    ListAbonos.Clear();
                    gcAbonos.RefreshDataSource();
                    gcDetallesSeparado.RefreshDataSource();
                    return;
                }
                goError:
                ClFunciones.MensajeError("Lo sentimos, ha ocurrido un error.");
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
                    
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtProd.ValorTextBox = "";
            txtValUnit.ValorTextBox = "";
            txtCantidad.ValorTextBox = "";
            txtValto.ValorTextBox = "";
            txtCelCli.ValorTextBox = "";
            txtProd.Focus();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {

        }

        private void LlenarControlProds()
        {
            try
            {
                string sql = "SELECT Sec AS Codigo, Nombre AS Descripcion FROM Productos";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                txtProd.DataTable = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void btnAddAbono_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtValAbono.ValorTextBox == "" || Convert.ToDecimal(txtValAbono.ValorTextBox) <= 0) return;
                if (ListAbonos.Sum(x => x.ValAbono) + Convert.ToDecimal(txtValAbono.ValorTextBox) > ListaSeparados.Sum(x => x.ValTo))
                {
                    ClFunciones.MensajeError("La sumatoria de abonos supera el valor del separado.");
                    return;
                }
                AbonosSeparadoET Abono = new AbonosSeparadoET()
                {
                    FechaAbono = DateTime.Now,
                    IdSeparado = Separado.Sec,
                    Sec = 0,
                    ValAbono = Convert.ToDecimal(txtValAbono.ValorTextBox)
                };
                ListAbonos.Add(Abono);
                gcAbonos.RefreshDataSource();
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }

        }

        private void txtProd_SaleControl(object sender, EventArgs e)
        {
            try
            {
                if (txtProd.ValorTextBox != "")
                {
                    string sql = "SELECT PrecioVenta FROM PRODUCTOS WHERE SEC = " + txtProd.ValorTextBox;
                    DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    if (dt.Rows.Count > 0)
                    {
                        txtValUnit.ValorTextBox = dt.Rows[0][0].ToString();
                    }
                }
                else
                {
                    txtValUnit.ValorTextBox = "";
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void ucLabelTextBox1_SaleControl(object sender, EventArgs e)
        {
            try
            {
                if (txtCantidad.ValorTextBox != "" && txtValUnit.ValorTextBox != "")
                {
                    txtValto.ValorTextBox = (Convert.ToInt32(txtValUnit.ValorTextBox) * Convert.ToInt32(txtCantidad.ValorTextBox)).ToString();
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                if (!isNewElement) return;
                ListaSeparados.Remove(ListaSeparados.Where(x => x.IdProd == Convert.ToInt32(txtProd.ValorTextBox)).FirstOrDefault());
                gcDetallesSeparado.RefreshDataSource();
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }

        }

        private void repositoryItemButtonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                ListAbonos.Remove((AbonosSeparadoET)gvAbonos.GetFocusedRow());
                gcAbonos.RefreshDataSource();
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }

        }
    }
}
