﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmProductos : Form
    {
        int idProd = -1;
        bool estaActualizando = false;
        public FrmProductos()
        {
            InitializeComponent();
        }

        private void FrmProductos_Load(object sender, EventArgs e)
        {
            CargarDatosCOntrol();
            CrearGrilla();
            LlenarGrilla();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                if (estaActualizando)
                {
                    bool res = ActProducto(idProd,txtNom.ValorTextBox, 
                        txtDes.ValorTextBox,Convert.ToDecimal(txtCosto.ValorTextBox),
                        Convert.ToDecimal(txtVenta.ValorTextBox),DateTime.Now,
                        Convert.ToInt32(txtCat.ValorTextBox));

                    if (res)
                    {

                        ClFunciones.MensajeExitoso("Producto actualizado correctamente.");
                        LlenarGrilla();
                        LimpiarCampos();
                    }
                    else
                    {
                        ClFunciones.MensajeError("Lo sentimos, ha ocurrido un error.");
                    }
                }
                else
                {
                    int Sec = ClFunciones.TraeSiguienteSecuencial("Productos", "Sec", ClConexion.clConexion.Conexion);
                    if (Sec>0)
                    {
                        bool res = CrearProducto(Sec,txtNom.ValorTextBox, txtDes.ValorTextBox,
                            Convert.ToDecimal(txtCosto.ValorTextBox), 
                            Convert.ToDecimal(txtVenta.ValorTextBox),DateTime.Now, 
                            Convert.ToInt32(txtCat.ValorTextBox));
                        if (res)
                        {
                            ClFunciones.MensajeExitoso("Producto creado correctamente.");
                            LlenarGrilla();
                            LimpiarCampos();
                        }
                        else
                        {
                            ClFunciones.MensajeError("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                    else
                    {
                        ClFunciones.MensajeError("Lo sentimos, ha ocurrido un error.");
                    }
                }
            }
            
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CargarDatosCOntrol()
        {
            try
            {
                string sql = "SELECT IdCat AS Codigo, NomCat AS Descripcion FROM Categorias";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                txtCat.DataTable = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void CrearGrilla()
        {
            try
            {
                gvProd = GrillaDevExpress.CrearGrilla(false, false, "");
                gvProd.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
                gvProd.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Id", ancho: 20));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Nombre", "Nombre"));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Descripcion", "Descripción", ancho: 120));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("Cantidad", "Existencia", ancho:50));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("PrecioCosto", "P. costo",tipo:DevExpress.Utils.FormatType.Numeric,formato:"C2", ancho:50));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("PrecioVenta", "P. venta", tipo: DevExpress.Utils.FormatType.Numeric, formato: "C2", ancho: 50));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("FechaReg", "Creado", tipo: DevExpress.Utils.FormatType.DateTime, formato: "dd/MM/yyyy hh:mm tt", ancho: 80));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("FechaMod", "Modificado", tipo: DevExpress.Utils.FormatType.DateTime, formato: "dd/MM/yyyy hh:mm tt", ancho: 80));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("IdCategoria", "", visible:false));
                gvProd.Columns.Add(GrillaDevExpress.CrearColumna("NomCat", "Categoria", ancho: 80));
                gvProd.OptionsBehavior.Editable = true;
                gvProd.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvProd.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvProd.OptionsCustomization.AllowColumnResizing = true;
                gcProd.MainView = gvProd;
                this.gvProd.DoubleClick += new System.EventHandler(this.gvProd_DoubleClick);
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private void gvProd_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
                if (numFila > -1)
                {
                    idProd = Convert.ToInt32(gvProd.GetDataRow(numFila)["Sec"]);
                    txtNom.ValorTextBox = gvProd.GetDataRow(numFila)["Nombre"].ToString();
                    txtDes.ValorTextBox = gvProd.GetDataRow(numFila)["Descripcion"].ToString();
                    txtCan.ValorTextBox = gvProd.GetDataRow(numFila)["Cantidad"].ToString();
                    txtCosto.ValorTextBox = gvProd.GetDataRow(numFila)["PrecioCosto"].ToString();
                    txtVenta.ValorTextBox = gvProd.GetDataRow(numFila)["PrecioVenta"].ToString();
                    txtCat.ValorTextBox = gvProd.GetDataRow(numFila)["IdCategoria"].ToString();
                    txtCat.BuscarDato(gvProd.GetDataRow(numFila)["IdCategoria"].ToString());
                    txtCat.Focus();
                    estaActualizando = true;
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
           
        }

        private void LlenarGrilla()
        {
            try
            {
                string sql = "SELECT PD.Sec, PD.Nombre, PD.Descripcion, PD.Cantidad, PD.PrecioCosto,  " +
                " PD.PrecioVenta, PD.FechaReg, PD.FechaMod, PD.IdCategoria, CT.NomCat " +
                " FROM Productos PD " +
                " INNER JOIN Categorias CT ON PD.IdCategoria = CT.IdCat";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                gcProd.DataSource = dt;
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
            }
        }

        private bool CrearProducto(int Sec,
                                string Nombre,
                                string Descripcion,
                                decimal PrecioCosto,
                                decimal PrecioVenta,
                                DateTime FechaReg,
                                int IdCategoria
                                )
        {
            try
            {
                string sql = String.Format("INSERT INTO Productos (Sec, Nombre, Descripcion, PrecioCosto, PrecioVenta, FechaReg, IdCategoria)  " +
                " VALUES({0}, '{1}', '{2}', {3}, {4}, '{5:dd/MM/yyyy HH:mm:ss}', {6})",
                Sec,Nombre,Descripcion, PrecioCosto, PrecioVenta,FechaReg,IdCategoria);
                string res = ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                if (res == "si")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
                return false;
            }
        }

        private bool ActProducto(int Sec,
                               string Nombre,
                               string Descripcion,
                               decimal PrecioCosto,
                               decimal PrecioVenta,
                               DateTime FechaMod,
                               int IdCategoria
                               )
        {
            try
            {
                string sql = string.Format("UPDATE Productos SET Nombre = '{0}', Descripcion='{1}', " +
                    " PrecioCosto={2}, PrecioVenta={3}, FechaMod='{4:dd/MM/yyyy HH:mm:ss}', IdCategoria={5} WHERE Sec = {6}",
                    Nombre, Descripcion, PrecioCosto, PrecioVenta, FechaMod, IdCategoria, Sec);
                string res = ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                if (res == "si")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ClFunciones.MensajeError(ex.Message);
                return false;
            }
        }
        private void LimpiarCampos()
        {
            txtCat.ValorTextBox = "";
            txtNom.ValorTextBox = "";
            txtCan.ValorTextBox = "";
            txtDes.ValorTextBox = "";
            txtCosto.ValorTextBox = "";
            txtVenta.ValorTextBox = "";
            estaActualizando = false;
            idProd = -1;
            txtCat.Focus();
        }

        private bool ValidarCampos()
        {
            if (txtCat.ValorTextBox =="")
            {
                ClFunciones.MensajeError("Debe seleccionar la categoría.");
                txtCat.Focus();
                return false;
            }
            else if (txtNom.ValorTextBox =="")
            {
                ClFunciones.MensajeError("Debe digitar el nombre del producto.");
                txtNom.Focus();
                return false;
            }
            else if (txtDes.ValorTextBox == "")
            {
                ClFunciones.MensajeError("Debe digitar la descripción del producto.");
                txtDes.Focus();
                return false;
            }
            else if (txtCosto.ValorTextBox == "")
            {
                ClFunciones.MensajeError("Debe digitar el costo del producto.");
                txtCosto.Focus();
                return false;
            }
            else if (txtVenta.ValorTextBox == "")
            {
                ClFunciones.MensajeError("Debe digitar el valor venta del producto.");
                txtVenta.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
            LlenarGrilla();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenarGrilla();
            CargarDatosCOntrol();
        }
    }
}
