﻿using Colegio.Clases;
using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Colegio.Vistas
{
    public partial class FrmServidores : FrmBase
    {
        string serv = "";
        int secServ = -1;
        TextBox _txt;
        public FrmServidores(ref TextBox txt)
        {
            InitializeComponent();
            _txt = txt;
        }

        private void btnAddServ_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtServ.Text != "") { ClFunciones.listaServidores.Add(txtServ.Text); txtServ.Text = ""; }
                ClFunciones.AgregaServidorALista(ClFunciones.listaServidores.ToArray());
                lbxServ.DataSource = new string[0];
                lbxServ.DataSource = ClFunciones.ObtenerListaDeServidores();
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }          
        }

        private void btnEliminaServ_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtServ.Text == "") { return; }
                ClFunciones.listaServidores.Remove(ClFunciones.listaServidores[secServ]);
                ClFunciones.AgregaServidorALista(ClFunciones.listaServidores.ToArray());
                this.serv = "";
                this.secServ = -1;
                txtServ.Text = "";
                lbxServ.DataSource = ClFunciones.ObtenerListaDeServidores();
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void FrmServidores_Load(object sender, EventArgs e)
        {           
            try
            {
                lbxServ.DataSource = ClFunciones.ObtenerListaDeServidores();
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void lbxServ_Click(object sender, EventArgs e)
        {
            try
            {
                serv = lbxServ.GetItemText(lbxServ.SelectedItem);
                secServ = lbxServ.SelectedIndex;
                txtServ.Text = serv;
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void lbxServ_DoubleClick(object sender, EventArgs e)
        {
            _txt.Text = lbxServ.GetItemText(lbxServ.SelectedItem);
            _txt.Focus();
            this.Close();
        }

        private void lblCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
