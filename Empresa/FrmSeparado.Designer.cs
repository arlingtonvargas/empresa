﻿namespace Empresa
{
    partial class FrmSeparado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSeparado));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCelCli = new dll.Controles.ucLabelTextBox();
            this.dtpFechaFinSep = new dll.Controles.ucLabelDateTime();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtValUnit = new dll.Controles.ucLabelTextBox();
            this.txtCantidad = new dll.Controles.ucLabelTextBox();
            this.txtValto = new dll.Controles.ucLabelTextBox();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.dtpFechaSep = new dll.Controles.ucLabelDateTime();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.txtProd = new dll.Controles.ucBusqueda();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcDetallesSeparado = new DevExpress.XtraGrid.GridControl();
            this.separadosDetalleVistaListETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvDetallesSeparado = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdProd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomProd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValProd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCant = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaRegistro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtValAbono = new dll.Controles.ucLabelTextBox();
            this.gcAbonos = new DevExpress.XtraGrid.GridControl();
            this.abonosSeparadoListETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvAbonos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSec1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdSeparado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaAbono = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValAbono = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btnAddAbono = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetallesSeparado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separadosDetalleVistaListETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetallesSeparado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAbonos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abonosSeparadoListETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAbonos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemTextEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemTextEdit2.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemTextEdit3.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemTextEdit4.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemTextEdit5.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit5.Mask.EditMask = "C2";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.tableLayoutPanel2);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(855, 113);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Información de producto";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 240F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 236F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.95594F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.04406F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel2.Controls.Add(this.txtCelCli, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.dtpFechaFinSep, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.dtpFechaSep, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnSalir, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnGuardar, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtProd, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnLimpiar, 5, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(5, 23);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(845, 85);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // txtCelCli
            // 
            this.txtCelCli.AnchoTitulo = 100;
            this.tableLayoutPanel2.SetColumnSpan(this.txtCelCli, 3);
            this.txtCelCli.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCelCli.EditMask = "";
            this.txtCelCli.Location = new System.Drawing.Point(479, 57);
            this.txtCelCli.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCelCli.MensajeDeAyuda = null;
            this.txtCelCli.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCelCli.Name = "txtCelCli";
            this.txtCelCli.PermiteSoloNumeros = true;
            this.txtCelCli.Size = new System.Drawing.Size(246, 24);
            this.txtCelCli.TabIndex = 7;
            this.txtCelCli.TextoTitulo = "Celular cliente :";
            this.txtCelCli.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtCelCli.ValorTextBox = "";
            // 
            // dtpFechaFinSep
            // 
            this.dtpFechaFinSep.AnchoTitulo = 120;
            this.dtpFechaFinSep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpFechaFinSep.Location = new System.Drawing.Point(243, 57);
            this.dtpFechaFinSep.MensajeDeAyuda = null;
            this.dtpFechaFinSep.Name = "dtpFechaFinSep";
            this.dtpFechaFinSep.Size = new System.Drawing.Size(230, 25);
            this.dtpFechaFinSep.TabIndex = 6;
            this.dtpFechaFinSep.TextoTitulo = "Fecha fin Separado :";
            this.dtpFechaFinSep.ValorDateTime = new System.DateTime(2018, 11, 25, 19, 18, 56, 681);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel1, 5);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 240F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.txtValUnit, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCantidad, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtValto, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAdd, 3, 0);
            this.tableLayoutPanel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 27);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(728, 27);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // txtValUnit
            // 
            this.txtValUnit.AnchoTitulo = 107;
            this.txtValUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtValUnit.EditMask = "";
            this.txtValUnit.Location = new System.Drawing.Point(3, 3);
            this.txtValUnit.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtValUnit.MensajeDeAyuda = null;
            this.txtValUnit.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtValUnit.Name = "txtValUnit";
            this.txtValUnit.PermiteSoloNumeros = true;
            this.txtValUnit.Size = new System.Drawing.Size(234, 24);
            this.txtValUnit.TabIndex = 1;
            this.txtValUnit.TextoTitulo = "Valor unit :";
            this.txtValUnit.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtValUnit.ValorTextBox = "";
            // 
            // txtCantidad
            // 
            this.txtCantidad.AnchoTitulo = 60;
            this.txtCantidad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCantidad.EditMask = "";
            this.txtCantidad.Location = new System.Drawing.Point(243, 3);
            this.txtCantidad.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCantidad.MensajeDeAyuda = null;
            this.txtCantidad.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.PermiteSoloNumeros = true;
            this.txtCantidad.Size = new System.Drawing.Size(119, 24);
            this.txtCantidad.TabIndex = 2;
            this.txtCantidad.TextoTitulo = "Cant :";
            this.txtCantidad.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtCantidad.ValorTextBox = "";
            this.txtCantidad.SaleControl += new dll.Controles.ucLabelTextBox.EventHandler(this.ucLabelTextBox1_SaleControl);
            // 
            // txtValto
            // 
            this.txtValto.AnchoTitulo = 100;
            this.txtValto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtValto.EditMask = "";
            this.txtValto.Enabled = false;
            this.txtValto.Location = new System.Drawing.Point(368, 3);
            this.txtValto.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtValto.MensajeDeAyuda = null;
            this.txtValto.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtValto.Name = "txtValto";
            this.txtValto.PermiteSoloNumeros = true;
            this.txtValto.Size = new System.Drawing.Size(327, 24);
            this.txtValto.TabIndex = 3;
            this.txtValto.TextoTitulo = "Valor total :";
            this.txtValto.TipoMask = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtValto.ValorTextBox = "";
            // 
            // btnAdd
            // 
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(701, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(24, 21);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dtpFechaSep
            // 
            this.dtpFechaSep.AnchoTitulo = 110;
            this.dtpFechaSep.Location = new System.Drawing.Point(3, 57);
            this.dtpFechaSep.MensajeDeAyuda = null;
            this.dtpFechaSep.Name = "dtpFechaSep";
            this.dtpFechaSep.Size = new System.Drawing.Size(234, 24);
            this.dtpFechaSep.TabIndex = 5;
            this.dtpFechaSep.TextoTitulo = "Fecha Separado :";
            this.dtpFechaSep.ValorDateTime = new System.DateTime(2018, 11, 25, 19, 18, 56, 681);
            // 
            // btnSalir
            // 
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(731, 57);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(111, 25);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(731, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(111, 21);
            this.btnGuardar.TabIndex = 5;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtProd
            // 
            this.txtProd.AnchoTextBox = 100;
            this.txtProd.AnchoTitulo = 110;
            this.tableLayoutPanel2.SetColumnSpan(this.txtProd, 5);
            this.txtProd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtProd.Location = new System.Drawing.Point(0, 0);
            this.txtProd.Margin = new System.Windows.Forms.Padding(0);
            this.txtProd.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtProd.MensajeDeAyuda = null;
            this.txtProd.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtProd.Name = "txtProd";
            this.txtProd.PermiteSoloNumeros = false;
            this.txtProd.Size = new System.Drawing.Size(728, 28);
            this.txtProd.TabIndex = 1;
            this.txtProd.TextoTitulo = "Producto :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtProd.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtProd.ValorTextBox = "";
            this.txtProd.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtProd_SaleControl);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(731, 30);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(111, 21);
            this.btnLimpiar.TabIndex = 6;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Location = new System.Drawing.Point(12, 131);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gcDetallesSeparado);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(855, 362);
            this.splitContainerControl1.SplitterPosition = 496;
            this.splitContainerControl1.TabIndex = 10;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gcDetallesSeparado
            // 
            this.gcDetallesSeparado.DataSource = this.separadosDetalleVistaListETBindingSource;
            this.gcDetallesSeparado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDetallesSeparado.Location = new System.Drawing.Point(0, 0);
            this.gcDetallesSeparado.MainView = this.gvDetallesSeparado;
            this.gcDetallesSeparado.Name = "gcDetallesSeparado";
            this.gcDetallesSeparado.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gcDetallesSeparado.Size = new System.Drawing.Size(496, 362);
            this.gcDetallesSeparado.TabIndex = 0;
            this.gcDetallesSeparado.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetallesSeparado});
            // 
            // separadosDetalleVistaListETBindingSource
            // 
            this.separadosDetalleVistaListETBindingSource.DataSource = typeof(Empresa.ET.SeparadosDetalleVistaListET);
            // 
            // gvDetallesSeparado
            // 
            this.gvDetallesSeparado.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSec,
            this.colIdProd,
            this.colNomProd,
            this.colValProd,
            this.colCant,
            this.colValTo,
            this.colFechaRegistro,
            this.gridColumn1});
            this.gvDetallesSeparado.GridControl = this.gcDetallesSeparado;
            this.gvDetallesSeparado.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValTo", null, "(Total : {0:#.##})")});
            this.gvDetallesSeparado.Name = "gvDetallesSeparado";
            this.gvDetallesSeparado.OptionsView.ShowFooter = true;
            this.gvDetallesSeparado.OptionsView.ShowGroupPanel = false;
            // 
            // colSec
            // 
            this.colSec.FieldName = "Sec";
            this.colSec.Name = "colSec";
            this.colSec.OptionsColumn.AllowEdit = false;
            this.colSec.OptionsColumn.AllowFocus = false;
            // 
            // colIdProd
            // 
            this.colIdProd.FieldName = "IdProd";
            this.colIdProd.Name = "colIdProd";
            this.colIdProd.OptionsColumn.AllowEdit = false;
            this.colIdProd.OptionsColumn.AllowFocus = false;
            this.colIdProd.Visible = true;
            this.colIdProd.VisibleIndex = 1;
            this.colIdProd.Width = 51;
            // 
            // colNomProd
            // 
            this.colNomProd.FieldName = "NomProd";
            this.colNomProd.Name = "colNomProd";
            this.colNomProd.OptionsColumn.AllowEdit = false;
            this.colNomProd.OptionsColumn.AllowFocus = false;
            this.colNomProd.Visible = true;
            this.colNomProd.VisibleIndex = 0;
            this.colNomProd.Width = 196;
            // 
            // colValProd
            // 
            this.colValProd.DisplayFormat.FormatString = "C2";
            this.colValProd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValProd.FieldName = "ValProd";
            this.colValProd.Name = "colValProd";
            this.colValProd.OptionsColumn.AllowEdit = false;
            this.colValProd.OptionsColumn.AllowFocus = false;
            this.colValProd.Visible = true;
            this.colValProd.VisibleIndex = 2;
            this.colValProd.Width = 74;
            // 
            // colCant
            // 
            this.colCant.FieldName = "Cant";
            this.colCant.Name = "colCant";
            this.colCant.OptionsColumn.AllowEdit = false;
            this.colCant.OptionsColumn.AllowFocus = false;
            this.colCant.Visible = true;
            this.colCant.VisibleIndex = 3;
            this.colCant.Width = 33;
            // 
            // colValTo
            // 
            this.colValTo.DisplayFormat.FormatString = "C2";
            this.colValTo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValTo.FieldName = "ValTo";
            this.colValTo.Name = "colValTo";
            this.colValTo.OptionsColumn.AllowEdit = false;
            this.colValTo.OptionsColumn.AllowFocus = false;
            this.colValTo.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValTo", "SUMA={0:C2}")});
            this.colValTo.Visible = true;
            this.colValTo.VisibleIndex = 4;
            this.colValTo.Width = 124;
            // 
            // colFechaRegistro
            // 
            this.colFechaRegistro.FieldName = "FechaRegistro";
            this.colFechaRegistro.Name = "colFechaRegistro";
            this.colFechaRegistro.OptionsColumn.AllowEdit = false;
            this.colFechaRegistro.OptionsColumn.AllowFocus = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Quitar";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.MaxWidth = 75;
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 5;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Quitar", 65, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtValAbono);
            this.panelControl1.Controls.Add(this.gcAbonos);
            this.panelControl1.Controls.Add(this.btnAddAbono);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(354, 362);
            this.panelControl1.TabIndex = 11;
            // 
            // txtValAbono
            // 
            this.txtValAbono.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValAbono.AnchoTitulo = 70;
            this.txtValAbono.EditMask = "C2";
            this.txtValAbono.Location = new System.Drawing.Point(5, 6);
            this.txtValAbono.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtValAbono.MensajeDeAyuda = null;
            this.txtValAbono.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtValAbono.Name = "txtValAbono";
            this.txtValAbono.PermiteSoloNumeros = true;
            this.txtValAbono.Size = new System.Drawing.Size(203, 24);
            this.txtValAbono.TabIndex = 14;
            this.txtValAbono.TextoTitulo = "Abono :";
            this.txtValAbono.TipoMask = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtValAbono.ValorTextBox = "";
            // 
            // gcAbonos
            // 
            this.gcAbonos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcAbonos.DataSource = this.abonosSeparadoListETBindingSource;
            this.gcAbonos.Location = new System.Drawing.Point(5, 36);
            this.gcAbonos.MainView = this.gvAbonos;
            this.gcAbonos.Name = "gcAbonos";
            this.gcAbonos.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit2});
            this.gcAbonos.Size = new System.Drawing.Size(341, 321);
            this.gcAbonos.TabIndex = 9;
            this.gcAbonos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAbonos});
            // 
            // abonosSeparadoListETBindingSource
            // 
            this.abonosSeparadoListETBindingSource.DataSource = typeof(Empresa.ET.AbonosSeparadoListET);
            // 
            // gvAbonos
            // 
            this.gvAbonos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSec1,
            this.colIdSeparado,
            this.colFechaAbono,
            this.colValAbono,
            this.gridColumn2});
            this.gvAbonos.GridControl = this.gcAbonos;
            this.gvAbonos.Name = "gvAbonos";
            this.gvAbonos.OptionsView.ShowFooter = true;
            this.gvAbonos.OptionsView.ShowGroupPanel = false;
            // 
            // colSec1
            // 
            this.colSec1.FieldName = "Sec";
            this.colSec1.Name = "colSec1";
            this.colSec1.OptionsColumn.AllowEdit = false;
            this.colSec1.OptionsColumn.AllowFocus = false;
            // 
            // colIdSeparado
            // 
            this.colIdSeparado.FieldName = "IdSeparado";
            this.colIdSeparado.Name = "colIdSeparado";
            this.colIdSeparado.OptionsColumn.AllowEdit = false;
            this.colIdSeparado.OptionsColumn.AllowFocus = false;
            // 
            // colFechaAbono
            // 
            this.colFechaAbono.FieldName = "FechaAbono";
            this.colFechaAbono.Name = "colFechaAbono";
            this.colFechaAbono.OptionsColumn.AllowEdit = false;
            this.colFechaAbono.OptionsColumn.AllowFocus = false;
            this.colFechaAbono.Visible = true;
            this.colFechaAbono.VisibleIndex = 0;
            this.colFechaAbono.Width = 88;
            // 
            // colValAbono
            // 
            this.colValAbono.DisplayFormat.FormatString = "C2";
            this.colValAbono.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValAbono.FieldName = "ValAbono";
            this.colValAbono.Name = "colValAbono";
            this.colValAbono.OptionsColumn.AllowEdit = false;
            this.colValAbono.OptionsColumn.AllowFocus = false;
            this.colValAbono.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValAbono", "Total={0:C2}")});
            this.colValAbono.Visible = true;
            this.colValAbono.VisibleIndex = 1;
            this.colValAbono.Width = 230;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Quitar";
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn2.MaxWidth = 75;
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Quitar", 65, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // btnAddAbono
            // 
            this.btnAddAbono.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAbono.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddAbono.ImageOptions.Image")));
            this.btnAddAbono.Location = new System.Drawing.Point(214, 5);
            this.btnAddAbono.Margin = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.btnAddAbono.Name = "btnAddAbono";
            this.btnAddAbono.Size = new System.Drawing.Size(132, 25);
            this.btnAddAbono.TabIndex = 13;
            this.btnAddAbono.Text = "Agregar abono";
            this.btnAddAbono.Click += new System.EventHandler(this.btnAddAbono_Click);
            // 
            // FrmSeparado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 505);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmSeparado";
            this.Text = "Separados";
            this.Load += new System.EventHandler(this.FrmSeparados_Load);
            this.SizeChanged += new System.EventHandler(this.FrmSeparados_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDetallesSeparado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separadosDetalleVistaListETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetallesSeparado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcAbonos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abonosSeparadoListETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAbonos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private dll.Controles.ucLabelTextBox txtCelCli;
        private dll.Controles.ucLabelTextBox txtValto;
        private dll.Controles.ucLabelTextBox txtCantidad;
        private dll.Controles.ucBusqueda txtProd;
        private dll.Controles.ucLabelTextBox txtValUnit;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private dll.Controles.ucLabelDateTime dtpFechaFinSep;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private dll.Controles.ucLabelDateTime dtpFechaSep;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnAddAbono;
        private DevExpress.XtraGrid.GridControl gcAbonos;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAbonos;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colSec1;
        private DevExpress.XtraGrid.Columns.GridColumn colIdSeparado;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaAbono;
        private DevExpress.XtraGrid.Columns.GridColumn colValAbono;
        private DevExpress.XtraGrid.GridControl gcDetallesSeparado;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetallesSeparado;
        private System.Windows.Forms.BindingSource separadosDetalleVistaListETBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSec;
        private DevExpress.XtraGrid.Columns.GridColumn colIdProd;
        private DevExpress.XtraGrid.Columns.GridColumn colNomProd;
        private DevExpress.XtraGrid.Columns.GridColumn colValProd;
        private DevExpress.XtraGrid.Columns.GridColumn colCant;
        private DevExpress.XtraGrid.Columns.GridColumn colValTo;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaRegistro;
        private System.Windows.Forms.BindingSource abonosSeparadoListETBindingSource;
        private dll.Controles.ucLabelTextBox txtValAbono;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
    }
}