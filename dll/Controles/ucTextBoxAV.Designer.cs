﻿namespace dll.Controles
{
    partial class ucTextBoxAV
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.fProperties)).BeginInit();
            this.SuspendLayout();
           
            // 
            // ucTextBoxAV
            // 
            this.Enter += new System.EventHandler(this.ucTextBoxAV_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ucTextBoxAV_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ucTextBoxAV_KeyPress);
            this.Leave += new System.EventHandler(this.ucTextBoxAV_Leave);
            this.ResumeLayout(false);

        }

        #endregion

    }
}
