﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using dll.Common;
using dll.Common.ET;

namespace dll.Controles
{
    public partial class ucBusqueda : DevExpress.XtraEditors.XtraUserControl
    {
        public delegate void EventHandler(object sender, EventArgs e);
        public event EventHandler SaleControl;
        private bool _PermiteSoloNumeros;
        private string _MensajeDeAyuda;
        private int _AnchoTitulo = 100;
        private int _AnchoTextBox = 200;
        private string _ValorTextBox = "";
        private string _TextoTitulo = "Titulo";
        private bool YaConsulto = false;
        DataTable dt = new DataTable();
        public ucBusqueda()
        {
            InitializeComponent();
        }

        private TituloColsBusquedaET _TituloColsBusqueda = new TituloColsBusquedaET() { Codigo = "Código", Descripcion = "Descripción" };
        public TituloColsBusquedaET TituloColsBusqueda
        {
            get
            {
                return _TituloColsBusqueda;
            }
            set
            {
                _TituloColsBusqueda = value;
            }

        }

        [Category("Propiedades propias")]
        [Description("Datatable con la información que cargara en la bsqueda e internamente.")]
        public DataTable DataTable
        {
            get
            {
                return dt;
            }
            set
            {
                dt = value;                
            }
        }

        [Category("Propiedades propias")]
        [Description("Determina si el campo solo recibe numeros.")]
        public bool PermiteSoloNumeros
        {
            get
            {
                return _PermiteSoloNumeros;
            }
            set
            {
                _PermiteSoloNumeros = value;
            }
        }

        [Category("Propiedades propias")]
        [Description("Mensaje de ayuda.")]
        public string MensajeDeAyuda
        {
            get
            {
                return _MensajeDeAyuda;
            }
            set
            {
                _MensajeDeAyuda = value;
            }
        }

        [Category("Propiedades propias")]
        [Description("Ancho del titulo.")]
        public int AnchoTitulo
        {
            get
            {
                return _AnchoTitulo;
            }
            set
            {
                _AnchoTitulo = value;
                tableLayoutPanel1.ColumnStyles[0].Width = AnchoTitulo;

            }
        }

        [Category("Propiedades propias")]
        [Description("Ancho del textbox.")]
        public int AnchoTextBox
        {
            get
            {
                return _AnchoTextBox;
            }
            set
            {
                _AnchoTextBox = value;
                tableLayoutPanel1.ColumnStyles[1].Width = AnchoTextBox;
            }
        }

        [Category("Propiedades propias")]
        [Description("Valor del TextBox.")]
        public string ValorTextBox
        {
            get
            {
                return _ValorTextBox;
            }
            set
            {
                _ValorTextBox = value;
                txtCampo.Text = ValorTextBox;
            }
        }

        [Category("Propiedades propias")]
        [Description("Texto del titulo.")]
        public string TextoTitulo
        {
            get
            {
                return _TextoTitulo;
            }
            set
            {
                _TextoTitulo = value;
                lblTitulo.Text = TextoTitulo;
            }
        }

        private void txtCampo_Enter(object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (txt != null)
                {
                    txt.BackColor = dll.Common.Class.ClFuncionesdll.ColorFondoControles;
                    txt.SelectionStart = 0;
                    txt.SelectionLength = txt.Text.Length;
                    txt.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                    lblTitulo.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                    dll.Common.Class.ClFuncionesdll.BarStaticItemPrincipal.Caption = this.MensajeDeAyuda;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtCampo_Leave(object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (txt != null)
                {
                    txt.BackColor = Color.White;
                    txt.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular);
                    lblTitulo.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular);
                    //Txt.BorderStyle = BorderStyle.FixedSingle;
                }
                if (txtCampo.Text!="")
                {
                    if (!YaConsulto) { BuscarDato(txtCampo.Text); }
                    ValorTextBox = txtCampo.Text;
                }
                else
                {
                    lblDescripcion.Text = "";
                    YaConsulto = false;
                }
                if (SaleControl != null)
                    SaleControl(this, e);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void BuscarDato(string Filtro)
        {
            try
            {
                DataRow[] fila = DataTable.Select("Codigo = '" + Filtro+"'");
                if (fila.Length==1)
                {
                    lblDescripcion.Text = fila[0]["Descripcion"].ToString();
                    ValorTextBox = Filtro;
                    YaConsulto = true;
                }
                else
                {
                    lblDescripcion.Text = "No existen datos";
                    txtCampo.Text = "";
                    YaConsulto = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Excepcion");
            }
        }

        private void AbriBusqueda()
        {
            try
            {
                YaConsulto = false;
                FrmBusqueda frm = new FrmBusqueda(DataTable);
                frm.TituloColsBusqueda = TituloColsBusqueda;
                frm.ShowDialog();
                dll.Common.ET.SeleccionBusquedaET SeleccionBusqueda = new dll.Common.ET.SeleccionBusquedaET();
                SeleccionBusqueda = frm.SeleccionBusqueda;
                if (SeleccionBusqueda.Codigo != "")
                {
                    txtCampo.Text = SeleccionBusqueda.Codigo;
                    lblDescripcion.Text = SeleccionBusqueda.Descripcion;
                    YaConsulto = true;
                }
            }
            catch (Exception ex)
            {


            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            AbriBusqueda();
        }

        private void ucBusqueda_Enter(object sender, EventArgs e)
        {
            txtCampo.Focus();
        }

        private void txtCampo_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    YaConsulto = false;
                }else if (e.KeyChar == (char)Keys.Enter)
                {
                    e.Handled = true;
                    SendKeys.Send("{TAB}");
                }
                else if (e.KeyChar == (char)Keys.Escape)
                {
                    e.Handled = true;
                    SendKeys.Send("+{TAB}");
                }

                if (PermiteSoloNumeros)
                {
                    if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtCampo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Up)
                {
                    e.Handled = true;
                    SendKeys.Send("+{TAB}");
                }
                else if (e.KeyCode == Keys.Down)
                {
                    e.Handled = true;
                    SendKeys.Send("{TAB}");
                }
                else if (e.KeyCode == Keys.Right)
                {
                    AbriBusqueda();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
