﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace dll.Controles
{
    public partial class ucBtnAceptarAV : DevExpress.XtraEditors.XtraUserControl
    {
        public delegate void ButtonClickedEventHandler(object sender, EventArgs e);
        public event ButtonClickedEventHandler OnUserControlButtonClicked;    

        public ucBtnAceptarAV()
        {
            InitializeComponent();
            btnAceptar.Click += new EventHandler(OnButtonClicked);
        }

        private void OnButtonClicked(object sender, EventArgs e)
        {
            if (OnUserControlButtonClicked != null)
                OnUserControlButtonClicked(this, e);
        }

     
    }
}
