﻿namespace dll.Common
{
    partial class FrmBusqueda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBusqueda = new DevExpress.XtraEditors.LabelControl();
            this.gcBusqueda = new DevExpress.XtraGrid.GridControl();
            this.gvBusqueda = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblCerrar = new DevExpress.XtraEditors.LabelControl();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.gcBusqueda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBusqueda)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBusqueda
            // 
            this.lblBusqueda.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.lblBusqueda.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusqueda.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblBusqueda.Appearance.Options.UseBackColor = true;
            this.lblBusqueda.Appearance.Options.UseFont = true;
            this.lblBusqueda.Appearance.Options.UseForeColor = true;
            this.lblBusqueda.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblBusqueda.Location = new System.Drawing.Point(12, 44);
            this.lblBusqueda.Name = "lblBusqueda";
            this.lblBusqueda.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblBusqueda.Size = new System.Drawing.Size(526, 37);
            this.lblBusqueda.TabIndex = 0;
            this.lblBusqueda.TextChanged += new System.EventHandler(this.lblBusqueda_TextChanged);
            // 
            // gcBusqueda
            // 
            this.gcBusqueda.Location = new System.Drawing.Point(12, 87);
            this.gcBusqueda.MainView = this.gvBusqueda;
            this.gcBusqueda.Name = "gcBusqueda";
            this.gcBusqueda.Size = new System.Drawing.Size(526, 501);
            this.gcBusqueda.TabIndex = 1;
            this.gcBusqueda.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBusqueda});
            // 
            // gvBusqueda
            // 
            this.gvBusqueda.GridControl = this.gcBusqueda;
            this.gvBusqueda.Name = "gvBusqueda";
          
            // 
            // lblCerrar
            // 
            this.lblCerrar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblCerrar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCerrar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCerrar.Appearance.Options.UseBackColor = true;
            this.lblCerrar.Appearance.Options.UseFont = true;
            this.lblCerrar.Appearance.Options.UseForeColor = true;
            this.lblCerrar.Appearance.Options.UseTextOptions = true;
            this.lblCerrar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblCerrar.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCerrar.Location = new System.Drawing.Point(508, 1);
            this.lblCerrar.Name = "lblCerrar";
            this.lblCerrar.Size = new System.Drawing.Size(39, 28);
            this.lblCerrar.TabIndex = 29;
            this.lblCerrar.Text = "X";
            this.lblCerrar.Click += new System.EventHandler(this.lblCerrar_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(0, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblTitulo.Size = new System.Drawing.Size(563, 32);
            this.lblTitulo.TabIndex = 28;
            this.lblTitulo.Text = "Busqueda avanzada";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 599);
            this.panel1.TabIndex = 30;
            // 
            // FrmBusqueda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 600);
            this.Controls.Add(this.lblCerrar);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcBusqueda);
            this.Controls.Add(this.lblBusqueda);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "FrmBusqueda";
            this.Text = "Busqueda avanzada";
            this.Load += new System.EventHandler(this.FrmBusqueda_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmBusqueda_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.gcBusqueda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBusqueda)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblBusqueda;
        private DevExpress.XtraGrid.GridControl gcBusqueda;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBusqueda;
        private DevExpress.XtraEditors.LabelControl lblCerrar;
        public System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panel1;
    }
}