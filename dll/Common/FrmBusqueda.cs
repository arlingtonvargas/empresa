﻿using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using dll.Common.Class;
using dll.Common.ET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dll.Common
{
    public partial class FrmBusqueda : FrmBase
    {
        bool buscando = false;
        DataTable dt = new DataTable();
        const string TEXTOLABEL = "Digite cualquier criterio de búsqueda";

        private TituloColsBusquedaET _TituloColsBusqueda = new TituloColsBusquedaET() { Codigo="Código",Descripcion="Descripción" };
        public TituloColsBusquedaET TituloColsBusqueda
        {
            get
            {
                return _TituloColsBusqueda;
            }
            set
            {
                _TituloColsBusqueda = value;
            }

        }

        private SeleccionBusquedaET _SeleccionBusqueda = new SeleccionBusquedaET();
        public SeleccionBusquedaET SeleccionBusqueda
        {
            get
            {
                return _SeleccionBusqueda;
            }
            set
            {
                _SeleccionBusqueda = value;
            }

        }
        public FrmBusqueda(DataTable _dt)
        {
            InitializeComponent();
            dt = _dt;
        }

        private void FrmBusqueda_Load(object sender, EventArgs e)
        {
            lblBusqueda.Text = TEXTOLABEL;
            CrearGrilla();
            gcBusqueda.DataSource = dt;
        }
        private void FiltrarDatos(string filtro)
        {
            DataView dv = new DataView(dt);
            dv.RowFilter = ClFuncionesdll.CrearFiltro(dt, filtro);
            gcBusqueda.DataSource = dv;
        }

        private void CrearGrilla()
        {
            try
            {
                gvBusqueda = GrillaDevExpress.CrearGrilla(false, true);
                gvBusqueda.Columns.Add(GrillaDevExpress.CrearColumna("Codigo", this.TituloColsBusqueda.Codigo));
                gvBusqueda.Columns.Add(GrillaDevExpress.CrearColumna("Descripcion", this.TituloColsBusqueda.Descripcion, ancho:400));
                gvBusqueda.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvBusqueda.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvBusqueda.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvBusqueda.OptionsCustomization.AllowColumnResizing = true;
                gcBusqueda.MainView = gvBusqueda;
                this.gvBusqueda.DoubleClick += new System.EventHandler(this.gvBusqueda_DoubleClick);
                this.gvBusqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvBusqueda_KeyPress);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void gvBusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Enter)
                {
                    //GridView view = sender as GridView;
                    //object obj = view.GetFocusedValue();
                    this.SeleccionBusqueda.Codigo = gvBusqueda.GetDataRow(gvBusqueda.FocusedRowHandle)["Codigo"].ToString();
                    this.SeleccionBusqueda.Descripcion = gvBusqueda.GetFocusedRowCellValue("Descripcion").ToString();
                    this.Close();
                }                
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void gvBusqueda_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                int fila = ClFuncionesdll.DobleClicSoreFila(sender, e);
                if (fila>-1)
                {
                    this.SeleccionBusqueda.Codigo = gvBusqueda.GetRowCellValue(fila, "Codigo").ToString();
                    this.SeleccionBusqueda.Descripcion = gvBusqueda.GetRowCellValue(fila, "Descripcion").ToString();
                    this.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

      

        private void FrmBusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)(Keys.Enter))
                {
                    return;
                }else if (e.KeyChar == (char)(Keys.Back))
                {
                    if (lblBusqueda.Text.Length > 0)
                    {
                        if (lblBusqueda.Text == TEXTOLABEL)
                        {
                            buscando = false;
                            return;
                        }
                        lblBusqueda.Text = lblBusqueda.Text.Remove(lblBusqueda.Text.Length - 1, 1);//  Mid(lblBusqueda.Text, 1, lblBusqueda.Text.Length - 1);
                    }
                     if (lblBusqueda.Text.Length == 0)
                    {                    
                        lblBusqueda.Text = TEXTOLABEL;                        
                        buscando = false;
                        gcBusqueda.DataSource = dt;
                        return;
                    }
                    gcBusqueda.DataSource = dt;
                    lblBusqueda_TextChanged(sender, e);
                }
                else if (e.KeyChar == (char)(Keys.Space))
                    lblBusqueda.Text = lblBusqueda.Text + " ";
                else if (e.KeyChar == (char)(Keys.Escape))
                {
                    this.Close();
                }
                else
                {
                    if (lblBusqueda.Text == TEXTOLABEL)
                    {
                        buscando = true;
                        lblBusqueda.Text = "";
                    }
                    lblBusqueda.Text = lblBusqueda.Text + e.KeyChar.ToString();
                }
                    



                //if (e.KeyChar == '\b')
                //{
                //    if (lblBusqueda.Text == TEXTOLABEL)
                //    {
                //        gcBusqueda.DataSource = _dt;
                //        return;
                //    }
                //    else if (lblBusqueda.Text == "")
                //    {
                //        lblBusqueda.Text = TEXTOLABEL;
                //        gcBusqueda.DataSource = _dt;
                //    }
                //    lblBusqueda.Text = lblBusqueda.Text.Remove(lblBusqueda.Text.Length-1,1);
                //    if (lblBusqueda.Text == "")
                //    {
                //        lblBusqueda.Text = TEXTOLABEL;
                //    }
                //}
                //else
                //{
                    

                //}
                //if (lblBusqueda.Text == TEXTOLABEL)
                //{
                //    lblBusqueda.Text = "";
                //}
                //if (char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar))
                //{
                //    lblBusqueda.Text += e.KeyChar.ToString();
                //    FiltrarDatos(lblBusqueda.Text);
                //}
                //else
                //{
                //    // char is neither letter or digit.
                //    // there are more methods you can use to determine the
                //    // type of char, e.g. char.IsSymbol
                //}
            }
            catch (Exception ex)
            {

            }
        }      

        private void lblCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblBusqueda_TextChanged(object sender, EventArgs e)
        {
            //lblBusqueda.Appearance.BackColor = Color.Bisque;
            //string Filtro = "";
            //DataRow[] Filas;
                //Filtro = CrearFiltro(dt, lblBusqueda.Text);
            try
            {
                if (lblBusqueda.Text.Length > 0)
                {
                    if (lblBusqueda.Text != TEXTOLABEL)
                        FiltrarDatos(lblBusqueda.Text);
                }
                else
                {
                   if(!buscando)
                        lblBusqueda.Text = TEXTOLABEL;
                }
            }
                catch (Exception ex)
            {
                //MensagedeError(ex.Message);
            }
        }

    }
}
