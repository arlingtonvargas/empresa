USE [BDFLOR]
GO
/****** Object:  Table [dbo].[Categorias]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categorias](
	[IdCat] [int] NOT NULL,
	[NomCat] [varchar](1500) NOT NULL,
 CONSTRAINT [PK_Categorias] PRIMARY KEY CLUSTERED 
(
	[IdCat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Gastos]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gastos](
	[IdGasto] [int] NOT NULL,
	[Descripcion] [varchar](5000) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[valor] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_Gastos] PRIMARY KEY CLUSTERED 
(
	[IdGasto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[Sec] [int] NOT NULL,
	[Nombre] [varchar](500) NULL,
	[Descripcion] [varchar](5000) NULL,
	[Cantidad] [int] NULL,
	[PrecioCosto] [numeric](18, 0) NULL,
	[PrecioVenta] [numeric](18, 0) NULL,
	[FechaReg] [datetime] NULL,
	[FechaMod] [datetime] NULL,
	[IdCategoria] [int] NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[Sec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegistroStock]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistroStock](
	[Sec] [int] NOT NULL,
	[FechaReg] [datetime] NOT NULL,
	[FechaEnvio] [datetime] NOT NULL,
 CONSTRAINT [PK_RegistroStock] PRIMARY KEY CLUSTERED 
(
	[Sec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RegistroStockDetalle]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistroStockDetalle](
	[IdRegistroStockDetalle] [int] NOT NULL,
	[IdRegistroStock] [int] NOT NULL,
	[IdProd] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[CantActual] [int] NULL,
	[CantFinal] [int] NULL,
 CONSTRAINT [PK_RegistroStockDetalle] PRIMARY KEY CLUSTERED 
(
	[IdRegistroStockDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[IdUsuario] [int] NOT NULL,
	[NombreUsu] [varchar](20) NOT NULL,
	[Password] [varchar](5000) NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VentaDetalle]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VentaDetalle](
	[IdVentaDetalle] [int] NOT NULL,
	[IdVenta] [int] NOT NULL,
	[IdProd] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[ValUni] [numeric](18, 3) NOT NULL,
	[ValTo] [numeric](18, 3) NOT NULL,
 CONSTRAINT [PK_VentaDetalle] PRIMARY KEY CLUSTERED 
(
	[IdVentaDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ventas]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ventas](
	[Sec] [int] NOT NULL,
	[FechaVenta] [datetime] NOT NULL,
	[FechaRegistro] [datetime] NOT NULL,
 CONSTRAINT [PK_Ventas] PRIMARY KEY CLUSTERED 
(
	[Sec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_Productos_Categorias] FOREIGN KEY([IdCategoria])
REFERENCES [dbo].[Categorias] ([IdCat])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_Productos_Categorias]
GO
ALTER TABLE [dbo].[RegistroStockDetalle]  WITH CHECK ADD  CONSTRAINT [FK_RegistroStockDetalle_Productos] FOREIGN KEY([IdProd])
REFERENCES [dbo].[Productos] ([Sec])
GO
ALTER TABLE [dbo].[RegistroStockDetalle] CHECK CONSTRAINT [FK_RegistroStockDetalle_Productos]
GO
ALTER TABLE [dbo].[RegistroStockDetalle]  WITH CHECK ADD  CONSTRAINT [FK_RegistroStockDetalle_RegistroStock] FOREIGN KEY([IdRegistroStock])
REFERENCES [dbo].[RegistroStock] ([Sec])
GO
ALTER TABLE [dbo].[RegistroStockDetalle] CHECK CONSTRAINT [FK_RegistroStockDetalle_RegistroStock]
GO
ALTER TABLE [dbo].[VentaDetalle]  WITH CHECK ADD  CONSTRAINT [FK_VentaDetalle_Productos] FOREIGN KEY([IdProd])
REFERENCES [dbo].[Productos] ([Sec])
GO
ALTER TABLE [dbo].[VentaDetalle] CHECK CONSTRAINT [FK_VentaDetalle_Productos]
GO
ALTER TABLE [dbo].[VentaDetalle]  WITH CHECK ADD  CONSTRAINT [FK_VentaDetalle_Ventas] FOREIGN KEY([IdVenta])
REFERENCES [dbo].[Ventas] ([Sec])
GO
ALTER TABLE [dbo].[VentaDetalle] CHECK CONSTRAINT [FK_VentaDetalle_Ventas]
GO
/****** Object:  StoredProcedure [dbo].[SP_ProductosReport_G]    Script Date: 15/11/2018 11:18:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arlington
-- Create date: 20-10-2018
-- Description:	Reporte de productos
-- =============================================
CREATE PROCEDURE [dbo].[SP_ProductosReport_G]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PD.Sec, PD.Nombre, PD.Descripcion, ISNULL(PD.Cantidad,0) AS Existencia, 
	PD.PrecioCosto, (ISNULL(PD.Cantidad,0)*PD.PrecioCosto) AS TotalCosto, 
	PD.PrecioVenta, (ISNULL(PD.Cantidad,0)*PD.PrecioVenta) AS TotalVenta  FROM Productos PD
END

GO
